﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuboid_calculator
{
    struct Cube
    {
        private double lenght, hight, width;

        public Cube(double lenght, double hight, double width)
        {
            this.lenght = lenght;
            this.hight = hight;
            this.width = width;
        }

        public double Lenght { get => lenght; set => lenght = value; }
        public double Hight { get => hight; set => hight = value; }
        public double Width { get => width; set => width = value; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
