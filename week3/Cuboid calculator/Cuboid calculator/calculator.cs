﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuboid_calculator
{
    static class Calculator
    {
        public static double calcArea(Cube cube)
        {
            double area;
            area = 2 * (cube.Width * cube.Hight + cube.Hight * cube.Lenght + cube.Width * cube.Lenght);
            return area;
        }

        public static double calcAreaM(Cube cube)
        {
            double area;
            area = calcArea(cube) * Math.Pow(10, -4);
            return area;
        }

        public static double calcVolume(Cube cube)
        {
            double volume;
            volume = cube.Hight * cube.Width * cube.Lenght;
            return volume;
        }

        public static double calcVolumeM(Cube cube)
        {
            double volume;
            volume = calcVolume(cube) * Math.Pow(10, -6);
            return volume;
        }

        public static double calcVolumeL(Cube cube)
        {
            double volume;
            volume = calcVolume(cube) * 0.001;
            return volume;
        }
    }
}
