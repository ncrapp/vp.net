﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuboid_calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            bool displayMenu = true;

            double lenght = 0;
            double hight = 0;
            double width = 0;

            Console.WriteLine("*** WELCOME TO CUBE CALCULATOR ***\n");
            Console.WriteLine("Please enter dimensions of Cube in cm\n");

            while (displayMenu)
            {
                (displayMenu, lenght, hight, width) = MainMenu();
            }

            Console.WriteLine("\nDimension entered: " + lenght + " - " + hight + " - " + width + "\n");

            Cube cube = new Cube(lenght, hight, width);            

            /*
            Cube cube = new Cube();            
            cube.Lenght = lenght;
            cube.Hight = hight;
            cube.Width = width;
            */
            

            Console.WriteLine("Cube area is:   " + Calculator.calcArea(cube) + "cm2");
            Console.WriteLine("Cube area is:   " + Calculator.calcAreaM(cube) + "m2");
            Console.WriteLine("Cube volume is: " + Calculator.calcVolume(cube) + "cm3");
            Console.WriteLine("Cube volume is: " + Calculator.calcVolumeM(cube) + "m3");
            Console.WriteLine("Cube volume is: " + Calculator.calcVolumeL(cube) + "L");

            Console.ReadLine();
        }

        private static (bool, double, double, double) MainMenu()
        {
            double lenght = 0;
            double hight = 0;
            double width = 0;

            Console.Write("Please enter Lenght: ");
            setValue(ref lenght);

            Console.Write("Please enter Hight: ");
            setValue(ref hight);

            Console.Write("Please enter Width: ");
            setValue(ref width);

            return (false, lenght, hight, width);
        }

        static void setValue(ref double value)
        {
            bool valid = false;
            while (!valid)
            {
                if (Double.TryParse(Console.ReadLine(), out value))
                {
                    if (value > 0)
                    {
                        valid = true;
                    }
                    else
                    {
                        Console.WriteLine("Number not valid");
                        Console.Write("Please enter correct value: ");
                    }
                }
                else
                {
                    Console.WriteLine("Number not valid");
                    Console.Write("Please enter correct value: ");
                }
            }
        }
    }
}
