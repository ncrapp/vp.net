﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CHECKPARSE
{
    class Program
    {
        static void Main(string[] args)
        {
            //CHECKPARSE("435345345364574574646");
            //testChecked();
            Console.ReadLine();
        }

        public static void CHECKPARSE(String input)
        {
            try
            {
                // DECLARING INT32 VARIABLE
                int val;
                // GETTING PARSED VALUE
                val = Int32.Parse(input);
                Console.WriteLine("'{0}' PARSED AS {1}", input, val);
            }
            catch (FormatException)
            {
                Console.WriteLine("CAN'T PARSED '{0}'", input);
            }
            catch (OverflowException e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static void testChecked()
        {
            try
            {
                checked
                {
                    // INCREMENT UP TO MAX.
                    int num = 0;

                    for (int i = 0; i < int.MaxValue; i++)
                    {
                        //Console.Write(num);
                        //Console.Clear();
                        num++;
                    }
                    // INCREMENT UP TO MAX AGAIN (ERROR).
                    for (int i = 0; i < int.MaxValue; i++)
                    {
                        num++;
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Overflow");
            }

        }
    }
}
