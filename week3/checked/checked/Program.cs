﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @checked
{
    class Program
    {
        static void Main(string[] args)
        {

            checked 
            {
                // INCREMENT UP TO MAX.
                int num = 0;
                for(int i = 0; i < int.MaxValue; i++)
{
                    num++;
                }
                // INCREMENT UP TO MAX AGAIN (ERROR).
                for(int i = 0; i < int.MaxValue; i++)
{
                    num++;
                }
            }
        }
    }
}
