﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Øvelse_2a
{
    class DieCup
    {
        private Die d1;
        private Die d2;
        private int sites;

        public DieCup()
        {
            d1 = new Die();
            d2 = new Die();
        }

        public DieCup(int sites)
        {
            this.sites = sites;
            d1 = new Die(sites);
            d2 = new Die(sites/2);
        }

        public DieCup(int sites, int sites2)
        {
            this.sites = sites;
            d1 = new Die(sites);
            d2 = new Die(sites2);
        }

        public int GetValue()
        {
            int totValue;
            totValue = d1.GetValue() + d2.GetValue();
            return totValue;
        }

        public void Roll()
        {
            d1.Roll();
            d2.Roll();
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append("**** Dies has been rolled ****\n");
            str.Append("**** Die 1 value: " + d1.GetValue() + "\n");
            str.Append("**** Die 2 value: " + d2.GetValue() + "\n");
            str.Append("------------------\n");
            str.Append("Total die value: " + GetValue());

            return str.ToString();
        }
    }
}
