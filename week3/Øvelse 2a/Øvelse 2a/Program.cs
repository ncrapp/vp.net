﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Øvelse_2a
{
    class Program
    {
        static void Main(string[] args)
        {
            //Øvelse 2a
            /*
            for(int i = 0; i<20; i++)
            {
                Die die = new Die();
                die.Roll();
                Console.WriteLine(die.GetValue());
            }
            */

            //DieCup dc = new DieCup();
            DieCup dc = new DieCup(20);
            dc.Roll();
            Console.WriteLine(dc.ToString());
            
            Console.ReadLine();
        }
    }
}
