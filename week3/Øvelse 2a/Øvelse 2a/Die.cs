﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Øvelse_2a
{
    class Die
    {
        private int value;
        private int sites;
        private static Random rand;
        //private Random rand;

        public Die()
        {
            value = 0;
            rand = new Random();
            sites = 6;
        }

        public Die(int sites)
        {
            value = 0;
            rand = new Random();
            this.sites = sites;
        }

        public int GetValue()
        {
            return value;
        }

        public void Roll()
        {
            value = rand.Next(1, sites+1);
        }
    }
}
