﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjectA
{
    public class Controller
    {
        private BookList bookList;
        private List<Book> books;
        private static volatile Controller controlInstance;

        private Controller()
        {
            bookList = BookList.Instance();
            books = new List<Book>();
        }

        public static Controller controllerInstance()
        {
            if(controlInstance == null)
            {
                controlInstance = new Controller();
            }
            return controlInstance;

        }

        public List<Book> LoadData()
        {
            string dir = Directory.GetCurrentDirectory();
            string filePath = dir + @"\" + "bookList.json";
            IDataLoader dataLoader = new JsonLoader(filePath);
            //JsonLoader dataLoader = new JsonLoader(filePath);

            try
            {
                bookList.SetBookList(dataLoader.Load());
                //bookList.SetBookList(dataLoader.LoadFromResource());
            }
            catch (Exception bex)
            {
                throw bex;
            }

            books = bookList.getBooks();
            return books;
        }

        public List<Book> LoadDefaultData()
        {
            List<Book> defaultBooks = new List<Book>();

            Book bookA = new Book(79.41, "Rasmus Klump i Pingonesien");
            Book bookB = new Book(50, "NaturTeknikfager.dk");
            Book bookC = new Book(99.4, "The Hobbits - the many lives of Bilbo, Sam, Merry and Pippin");
            Book bookE = new Book(79.41, "Broken Dimensions");
            Book bookF = new Book(50.264, "Fremtidens natur i Sønderborg Kommune");
            Book bookG = new Book(37.8, "Kunst af lyst");
            Book bookH = new Book(50.264, "By, sø, skov");

            defaultBooks.Add(bookA);
            defaultBooks.Add(bookB);
            defaultBooks.Add(bookC);
            defaultBooks.Add(bookE);
            defaultBooks.Add(bookF);
            defaultBooks.Add(bookG);
            defaultBooks.Add(bookH);

            bookList.SetBookList(defaultBooks);
            books = bookList.getBooks();
            return books;
        }

        public List<Book> GetBookList()
        {
            return this.bookList.getBooks();
        }

        public List<Book> GetOrgBookList()
        {
            return this.bookList.getOrgBooks();
        }

        public void AddBook(Book b)
        {
            bookList.AddBook(b);
        }

        #region Sort Methods
        public List<Book> QuickSort()
        {
            return bookList.QuickSort(books);
        }

        public List<Book> BubbleSort()
        {
            return bookList.BubbleSort(books);
        }

        public List<Book> InsertSort()
        {
            return bookList.InsertSort(books);
        }
        #endregion

        #region Search Methods
        public List<Book> BinarySearchID(double searchID)
        {
            List<Book> matches = BinarySearcher.BinarySearchByID(bookList.getBooks(), searchID);
            return matches;
        }

        public List<Book> SimpleSearchByID(double searchID)
        {
            List<Book> matches = SimpleSearch.SearchByID(bookList.getBooks(), searchID);
            return matches;
        }

        public List<Book> SimpleSearchByTitle(string bookTitle)
        {
            List<Book> matches = SimpleSearch.SearchByTitle(bookList.getBooks(), bookTitle);
            return matches;
        }
        #endregion
    }
}
