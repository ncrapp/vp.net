﻿namespace EksamensProjectA
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lv_Books = new System.Windows.Forms.ListView();
            this.lvCol_BookID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvCol_BookName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadBooklistToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.existToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnSort = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.grpBoxSort = new System.Windows.Forms.GroupBox();
            this.rdbSort = new System.Windows.Forms.RadioButton();
            this.rdbBubbleSort = new System.Windows.Forms.RadioButton();
            this.rdbQuickSort = new System.Windows.Forms.RadioButton();
            this.txtBookTitle = new System.Windows.Forms.TextBox();
            this.txtBookID = new System.Windows.Forms.TextBox();
            this.grpBoxSearch = new System.Windows.Forms.GroupBox();
            this.lblBookTitle = new System.Windows.Forms.Label();
            this.lvlBookID = new System.Windows.Forms.Label();
            this.txtBookIDSimple = new System.Windows.Forms.TextBox();
            this.grpBoxFastSearch = new System.Windows.Forms.GroupBox();
            this.btnClearAllSimple = new System.Windows.Forms.Button();
            this.lblBookTitleSimple = new System.Windows.Forms.Label();
            this.lvlBookIDSimple = new System.Windows.Forms.Label();
            this.txtBookTitleSimple = new System.Windows.Forms.TextBox();
            this.grpBoxSimpleActivate = new System.Windows.Forms.GroupBox();
            this.chkBoxSimple = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.grpBoxSort.SuspendLayout();
            this.grpBoxSearch.SuspendLayout();
            this.grpBoxFastSearch.SuspendLayout();
            this.grpBoxSimpleActivate.SuspendLayout();
            this.SuspendLayout();
            // 
            // lv_Books
            // 
            this.lv_Books.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvCol_BookID,
            this.lvCol_BookName});
            this.lv_Books.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_Books.FullRowSelect = true;
            this.lv_Books.GridLines = true;
            this.lv_Books.HideSelection = false;
            this.lv_Books.Location = new System.Drawing.Point(12, 123);
            this.lv_Books.MultiSelect = false;
            this.lv_Books.Name = "lv_Books";
            this.lv_Books.Size = new System.Drawing.Size(431, 217);
            this.lv_Books.TabIndex = 1;
            this.lv_Books.UseCompatibleStateImageBehavior = false;
            this.lv_Books.View = System.Windows.Forms.View.Details;
            // 
            // lvCol_BookID
            // 
            this.lvCol_BookID.Text = "BookID";
            this.lvCol_BookID.Width = 70;
            // 
            // lvCol_BookName
            // 
            this.lvCol_BookName.Text = "BookName";
            this.lvCol_BookName.Width = 340;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(641, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadBooklistToolStripMenuItem,
            this.existToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadBooklistToolStripMenuItem
            // 
            this.loadBooklistToolStripMenuItem.Name = "loadBooklistToolStripMenuItem";
            this.loadBooklistToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.loadBooklistToolStripMenuItem.Text = "Load Booklist";
            this.loadBooklistToolStripMenuItem.Click += new System.EventHandler(this.loadBooklistToolStripMenuItem_Click);
            // 
            // existToolStripMenuItem
            // 
            this.existToolStripMenuItem.Name = "existToolStripMenuItem";
            this.existToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.existToolStripMenuItem.Text = "Exit";
            this.existToolStripMenuItem.Click += new System.EventHandler(this.existToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "JSON Files (*.json)|*.json|All files (*.*)|*.*";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // btnSort
            // 
            this.btnSort.Location = new System.Drawing.Point(352, 55);
            this.btnSort.Name = "btnSort";
            this.btnSort.Size = new System.Drawing.Size(91, 43);
            this.btnSort.TabIndex = 3;
            this.btnSort.Text = "Sort";
            this.btnSort.UseVisualStyleBackColor = true;
            this.btnSort.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(458, 21);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(152, 27);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.button2_Click);
            // 
            // grpBoxSort
            // 
            this.grpBoxSort.Controls.Add(this.rdbSort);
            this.grpBoxSort.Controls.Add(this.rdbBubbleSort);
            this.grpBoxSort.Controls.Add(this.rdbQuickSort);
            this.grpBoxSort.Location = new System.Drawing.Point(13, 50);
            this.grpBoxSort.Name = "grpBoxSort";
            this.grpBoxSort.Size = new System.Drawing.Size(323, 48);
            this.grpBoxSort.TabIndex = 5;
            this.grpBoxSort.TabStop = false;
            this.grpBoxSort.Text = "Sort";
            // 
            // rdbSort
            // 
            this.rdbSort.AutoSize = true;
            this.rdbSort.Location = new System.Drawing.Point(220, 20);
            this.rdbSort.Name = "rdbSort";
            this.rdbSort.Size = new System.Drawing.Size(70, 17);
            this.rdbSort.TabIndex = 2;
            this.rdbSort.Text = "InsertSort";
            this.rdbSort.UseVisualStyleBackColor = true;
            this.rdbSort.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbBubbleSort
            // 
            this.rdbBubbleSort.AutoSize = true;
            this.rdbBubbleSort.Location = new System.Drawing.Point(126, 20);
            this.rdbBubbleSort.Name = "rdbBubbleSort";
            this.rdbBubbleSort.Size = new System.Drawing.Size(77, 17);
            this.rdbBubbleSort.TabIndex = 1;
            this.rdbBubbleSort.Text = "BubbleSort";
            this.rdbBubbleSort.UseVisualStyleBackColor = true;
            this.rdbBubbleSort.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbQuickSort
            // 
            this.rdbQuickSort.AutoSize = true;
            this.rdbQuickSort.Location = new System.Drawing.Point(32, 19);
            this.rdbQuickSort.Name = "rdbQuickSort";
            this.rdbQuickSort.Size = new System.Drawing.Size(72, 17);
            this.rdbQuickSort.TabIndex = 0;
            this.rdbQuickSort.Text = "QuickSort";
            this.rdbQuickSort.UseVisualStyleBackColor = true;
            this.rdbQuickSort.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // txtBookTitle
            // 
            this.txtBookTitle.Enabled = false;
            this.txtBookTitle.Location = new System.Drawing.Point(297, 25);
            this.txtBookTitle.Name = "txtBookTitle";
            this.txtBookTitle.Size = new System.Drawing.Size(133, 20);
            this.txtBookTitle.TabIndex = 6;
            // 
            // txtBookID
            // 
            this.txtBookID.Location = new System.Drawing.Point(94, 25);
            this.txtBookID.Name = "txtBookID";
            this.txtBookID.Size = new System.Drawing.Size(116, 20);
            this.txtBookID.TabIndex = 7;
            this.txtBookID.TextChanged += new System.EventHandler(this.txtBookID_TextChanged);
            // 
            // grpBoxSearch
            // 
            this.grpBoxSearch.Controls.Add(this.lblBookTitle);
            this.grpBoxSearch.Controls.Add(this.lvlBookID);
            this.grpBoxSearch.Controls.Add(this.txtBookID);
            this.grpBoxSearch.Controls.Add(this.txtBookTitle);
            this.grpBoxSearch.Controls.Add(this.btnSearch);
            this.grpBoxSearch.Location = new System.Drawing.Point(13, 356);
            this.grpBoxSearch.Name = "grpBoxSearch";
            this.grpBoxSearch.Size = new System.Drawing.Size(616, 57);
            this.grpBoxSearch.TabIndex = 8;
            this.grpBoxSearch.TabStop = false;
            this.grpBoxSearch.Text = "Binary Search";
            // 
            // lblBookTitle
            // 
            this.lblBookTitle.AutoSize = true;
            this.lblBookTitle.Enabled = false;
            this.lblBookTitle.Location = new System.Drawing.Point(239, 28);
            this.lblBookTitle.Name = "lblBookTitle";
            this.lblBookTitle.Size = new System.Drawing.Size(52, 13);
            this.lblBookTitle.TabIndex = 9;
            this.lblBookTitle.Text = "BookTitle";
            // 
            // lvlBookID
            // 
            this.lvlBookID.AutoSize = true;
            this.lvlBookID.Location = new System.Drawing.Point(47, 28);
            this.lvlBookID.Name = "lvlBookID";
            this.lvlBookID.Size = new System.Drawing.Size(43, 13);
            this.lvlBookID.TabIndex = 9;
            this.lvlBookID.Text = "BookID";
            // 
            // txtBookIDSimple
            // 
            this.txtBookIDSimple.Location = new System.Drawing.Point(7, 52);
            this.txtBookIDSimple.Name = "txtBookIDSimple";
            this.txtBookIDSimple.Size = new System.Drawing.Size(152, 20);
            this.txtBookIDSimple.TabIndex = 9;
            this.txtBookIDSimple.TextChanged += new System.EventHandler(this.txtBookIDSimple_TextChanged);
            // 
            // grpBoxFastSearch
            // 
            this.grpBoxFastSearch.Controls.Add(this.btnClearAllSimple);
            this.grpBoxFastSearch.Controls.Add(this.lblBookTitleSimple);
            this.grpBoxFastSearch.Controls.Add(this.lvlBookIDSimple);
            this.grpBoxFastSearch.Controls.Add(this.txtBookTitleSimple);
            this.grpBoxFastSearch.Controls.Add(this.txtBookIDSimple);
            this.grpBoxFastSearch.Enabled = false;
            this.grpBoxFastSearch.Location = new System.Drawing.Point(464, 117);
            this.grpBoxFastSearch.Name = "grpBoxFastSearch";
            this.grpBoxFastSearch.Size = new System.Drawing.Size(165, 223);
            this.grpBoxFastSearch.TabIndex = 10;
            this.grpBoxFastSearch.TabStop = false;
            this.grpBoxFastSearch.Text = "SimpleSearch";
            // 
            // btnClearAllSimple
            // 
            this.btnClearAllSimple.Location = new System.Drawing.Point(7, 179);
            this.btnClearAllSimple.Name = "btnClearAllSimple";
            this.btnClearAllSimple.Size = new System.Drawing.Size(152, 32);
            this.btnClearAllSimple.TabIndex = 13;
            this.btnClearAllSimple.Text = "Clear All";
            this.btnClearAllSimple.UseVisualStyleBackColor = true;
            this.btnClearAllSimple.Click += new System.EventHandler(this.btnClearAllSimple_Click);
            // 
            // lblBookTitleSimple
            // 
            this.lblBookTitleSimple.AutoSize = true;
            this.lblBookTitleSimple.Location = new System.Drawing.Point(7, 90);
            this.lblBookTitleSimple.Name = "lblBookTitleSimple";
            this.lblBookTitleSimple.Size = new System.Drawing.Size(55, 13);
            this.lblBookTitleSimple.TabIndex = 12;
            this.lblBookTitleSimple.Text = "Book Title";
            // 
            // lvlBookIDSimple
            // 
            this.lvlBookIDSimple.AutoSize = true;
            this.lvlBookIDSimple.Location = new System.Drawing.Point(7, 33);
            this.lvlBookIDSimple.Name = "lvlBookIDSimple";
            this.lvlBookIDSimple.Size = new System.Drawing.Size(43, 13);
            this.lvlBookIDSimple.TabIndex = 11;
            this.lvlBookIDSimple.Text = "BookID";
            // 
            // txtBookTitleSimple
            // 
            this.txtBookTitleSimple.Location = new System.Drawing.Point(7, 110);
            this.txtBookTitleSimple.Name = "txtBookTitleSimple";
            this.txtBookTitleSimple.Size = new System.Drawing.Size(152, 20);
            this.txtBookTitleSimple.TabIndex = 10;
            this.txtBookTitleSimple.TextChanged += new System.EventHandler(this.txtBookTitleSimple_TextChanged);
            // 
            // grpBoxSimpleActivate
            // 
            this.grpBoxSimpleActivate.Controls.Add(this.chkBoxSimple);
            this.grpBoxSimpleActivate.Location = new System.Drawing.Point(464, 50);
            this.grpBoxSimpleActivate.Name = "grpBoxSimpleActivate";
            this.grpBoxSimpleActivate.Size = new System.Drawing.Size(165, 48);
            this.grpBoxSimpleActivate.TabIndex = 11;
            this.grpBoxSimpleActivate.TabStop = false;
            this.grpBoxSimpleActivate.Text = "Activate SimpleSearch";
            // 
            // chkBoxSimple
            // 
            this.chkBoxSimple.AutoSize = true;
            this.chkBoxSimple.Location = new System.Drawing.Point(7, 25);
            this.chkBoxSimple.Name = "chkBoxSimple";
            this.chkBoxSimple.Size = new System.Drawing.Size(89, 17);
            this.chkBoxSimple.TabIndex = 0;
            this.chkBoxSimple.Text = "LinearSearch";
            this.chkBoxSimple.UseVisualStyleBackColor = true;
            this.chkBoxSimple.CheckedChanged += new System.EventHandler(this.chkBoxSimple_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 425);
            this.Controls.Add(this.grpBoxSimpleActivate);
            this.Controls.Add(this.grpBoxFastSearch);
            this.Controls.Add(this.grpBoxSearch);
            this.Controls.Add(this.btnSort);
            this.Controls.Add(this.grpBoxSort);
            this.Controls.Add(this.lv_Books);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aarhus Library Book Manager";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpBoxSort.ResumeLayout(false);
            this.grpBoxSort.PerformLayout();
            this.grpBoxSearch.ResumeLayout(false);
            this.grpBoxSearch.PerformLayout();
            this.grpBoxFastSearch.ResumeLayout(false);
            this.grpBoxFastSearch.PerformLayout();
            this.grpBoxSimpleActivate.ResumeLayout(false);
            this.grpBoxSimpleActivate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lv_Books;
        private System.Windows.Forms.ColumnHeader lvCol_BookID;
        private System.Windows.Forms.ColumnHeader lvCol_BookName;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadBooklistToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem existToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnSort;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.GroupBox grpBoxSort;
        private System.Windows.Forms.RadioButton rdbSort;
        private System.Windows.Forms.RadioButton rdbBubbleSort;
        private System.Windows.Forms.RadioButton rdbQuickSort;
        private System.Windows.Forms.TextBox txtBookTitle;
        private System.Windows.Forms.TextBox txtBookID;
        private System.Windows.Forms.GroupBox grpBoxSearch;
        private System.Windows.Forms.Label lblBookTitle;
        private System.Windows.Forms.Label lvlBookID;
        private System.Windows.Forms.TextBox txtBookIDSimple;
        private System.Windows.Forms.GroupBox grpBoxFastSearch;
        private System.Windows.Forms.Label lblBookTitleSimple;
        private System.Windows.Forms.Label lvlBookIDSimple;
        private System.Windows.Forms.TextBox txtBookTitleSimple;
        private System.Windows.Forms.GroupBox grpBoxSimpleActivate;
        private System.Windows.Forms.CheckBox chkBoxSimple;
        private System.Windows.Forms.Button btnClearAllSimple;
    }
}

