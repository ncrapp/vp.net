﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace EksamensProjectA
{
    class JsonLoader : AbstractDataLoader
    {
        private string jsonResult;
        private string filePath;
        private List<Book> bookList;
        StreamReader reader;

        public JsonLoader(string filePath)
        {
            bookList = new List<Book>();
            this.filePath = filePath;            
        }
        
        //Not working as intended and not implemented
        public List<Book> LoadFromResource()    
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Properties.Resources.bookList";

            try
            {
                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    string jsonResult = reader.ReadToEnd(); //Make string equal to full file
                    Debug.WriteLine(jsonResult);
                }
                bookList = JsonConvert.DeserializeObject<List<Book>>(jsonResult);
                Debug.WriteLine(bookList);
                foreach (Book b in bookList)
                {
                    Debug.WriteLine(b.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                reader.Close();
            }

            return bookList;
        }

        public override List<Book> Load()
        {
            try
            {
                if (File.Exists(filePath))
                {
                    using (reader = new StreamReader(filePath, true))
                    {
                        jsonResult = reader.ReadToEnd();
                        reader.Close();
                    }
                }
                bookList = JsonConvert.DeserializeObject<List<Book>>(jsonResult);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                reader.Close();
            }

            return bookList;
        }

        public override void Save(List<Book> booksSave)
        {
            jsonResult = JsonConvert.SerializeObject(booksSave, Formatting.Indented);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                using (var tw = new StreamWriter(filePath, true))
                {
                    tw.WriteLine(jsonResult.ToString());
                    tw.Close();
                }
            }
            else if (!File.Exists(filePath))
            {
                using (var tw = new StreamWriter(filePath, true))
                {
                    tw.WriteLine(jsonResult.ToString());
                    tw.Close();
                }
            }
        }
    }
}
