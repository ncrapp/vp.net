﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjectA
{
    [Serializable]
    class BookList
    {
        #region fields
        private List<Book> books;
        private List<Book> orgBookList; //List of books that will not be affected by sorting mechanisms
        private static volatile BookList booklistInstance;
        #endregion

        #region constructor
        private BookList()
        {
            books = new List<Book>();
            orgBookList = new List<Book>();
        }

        private BookList(List<Book> books)
        {
            this.books = books;
            this.orgBookList = books;
        }       
        #endregion

        public static BookList Instance()
        {
            if (booklistInstance == null)
            {
                booklistInstance = new BookList();
            }
            return booklistInstance;
        }
        
        public static BookList Instance(List<Book> books)
        {
            if (booklistInstance == null)
            {
                booklistInstance = new BookList(books);                   
            }
            return booklistInstance;
        }        

        public void SetBookList(List<Book> books)
        {
            this.books = books;
            //Copying the references to Book objects to a complete new List<Book> 
            //to maintain a original order of elements after any kind of sorting

            foreach (Book b in books) 
            {
                orgBookList.Add(b);
            }

        }

        public void AddBook(Book book)
        {
            this.books.Add(book);
            this.orgBookList.Add(book);
            //Sort udkommenteret da bookList<T> ikke skal sorteres ved demo da demo skal kunne vise den usorteret liste.
            //books.Sort();
            //books = QuickSort(books);
            //books = BubbleSort(books);
        }

        public List<Book> getBooks()
        {
            return books;
        }

        public List<Book> getOrgBooks()
        {
            return orgBookList;
        }

        public Book getBook(int index)
        {
            return books[index];
        }


        #region Sort methods

        public List<Book> QuickSort(List<Book> items)
        //QuickSort is not affecting the ingoing List<Book> as it creates a complete new List<Book> to which sorted books are added
        {
            if (items.Count <= 1)
            {
                return items;
            }

            List<Book> before = new List<Book>();
            List<Book> after = new List<Book>();

            Book pivot = items[0];

            for (int i = 1; i < items.Count; i++)
            {
                if (items[i].Id > pivot.Id)
                {
                    after.Add(items[i]);
                }
                else if (items[i].Id == pivot.Id)
                {
                    if (string.Compare(items[i].Title, pivot.Title) == -1)
                    {
                        before.Add(items[i]);
                    }
                    else
                    {
                        after.Add(items[i]);
                    }
                }
                else
                {
                    before.Add(items[i]);
                }
            }

            List<Book> result = new List<Book>();
            result.AddRange(QuickSort(before));
            result.Add(pivot);
            result.AddRange(QuickSort(after));

            return result;
        }

        public List<Book> BubbleSort(List<Book> items)
        //BubbleSort effects the original List<Book> as List<Book> points to input List<Book>
        {
            List<Book> result = items;
            Book temp;

            for (int j = 0; j <= result.Count - 2; j++)
            {
                for (int i = 0; i <= result.Count - 2; i++)
                {
                    if (result[i].Id > result[i + 1].Id)
                    {
                        temp = result[i + 1];
                        result[i + 1] = result[i];
                        result[i] = temp;
                    }
                    else if(result[i].Id == result[i + 1].Id)
                    {
                        if(string.Compare(result[i].Title, result[i + 1].Title) == 1)
                        {
                            temp = result[i + 1];
                            result[i + 1] = result[i];
                            result[i] = temp;
                        }
                    }
                }
            }
            return result;
        }

        public List<Book> InsertSort(List<Book> items)
        //InsertSort effects the original List<Book> as List<Book> points to input List<Book>
        {
            List<Book> result = items;
            int n = result.Count;            

            for (int i = 1; i < n; ++i)
            {
                Book bookKey = result[i];
                int j = i - 1;

                // Move elements of items[0..i-1], 
                // that are greater than key, 
                // to one position ahead of 
                // their current position 
                while (j >= 0 && result[j].Id > bookKey.Id || j >= 0 && result[j].Id == bookKey.Id && string.Compare(result[j].Title, bookKey.Title) == 1)
                {
                    result[j + 1] = result[j];
                    j--;
                }
                result[j + 1] = bookKey;
            }

            return result;
        }
        #endregion

        //Deep clone method - Not used
        public static List<T> CloneList<T>(List<T> oldList)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, oldList);
            stream.Position = 0;
            return (List<T>)formatter.Deserialize(stream);
        }

        public override string ToString()
        {
            string returnstring = "";

            foreach (Book book in getBooks())
            {
                returnstring += $"{book.ToString()} \n";
            }

            return returnstring;
        }
    }
}
