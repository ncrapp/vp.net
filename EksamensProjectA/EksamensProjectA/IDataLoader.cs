﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjectA
{
    interface IDataLoader
    {
        List<Book> BookList { get; }
        string Path { set; get; }
        string Credentials { set; get; }

        List<Book> Load();
        void Save(List<Book> books);
    }
}
