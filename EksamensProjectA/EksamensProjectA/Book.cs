﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjectA
{
    [Serializable]
    public class Book : IComparable<Book>
    {
        private double id;
        private string title;

        public double Id { get => id; }
        public string Title { get => title; }

        public Book(double id, string title)
        {
            this.id = id;
            this.title = title;
        }

        public int CompareTo(Book other)
        {
            //Implementing ICompare Interface
            //Returns integer 1, 0 or -1
            //=1: Greater than comparison Book Object
            //=0: equals comparison Book Object
            //-1: Lesser than comparison Book Object
            if (this.Id > other.Id)
            {
                return 1;
            }
            else if (this.Id < other.Id)
            {
                return -1;
            }
            else
            {
                if (string.Compare(this.Title, other.Title) == 1)
                {
                    return 1;
                }
                else if (string.Compare(this.Title, other.Title) == -1)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        public override string ToString()
        {
            string returnString = $"ID: {Id} - Title: {Title}";
            return returnString;
        }
    }
}
