﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjectA
{
    //Class not used as methods in subclasses has been refactored to static methods
    abstract class Searcher
    {
        public List<Book> SearchByID(List<Book> bookList, double target)
        {
            throw new NotImplementedException();
        }

        public List<Book> SearchByTitle(List<Book> bookList, double target)
        {
            throw new NotImplementedException();
        }
    }
}
