﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace EksamensProjectA
{
    public class BinarySearcher
    {
        // A recursive binary search  
        // function. It returns location 
        // of target in given list bookList[l..r]  
        // if present, otherwise -1 
        private static int binarySearch(List<Book> bookList, int left, int right, double target)
        {
            if (right < left)
                return -1;

            int mid = left + (right - left) / 2;

            // If the element is present  
            // at the middle itself 
            if (bookList[mid].Id == target)
                return mid;

            // If element is smaller than  
            // mid, then it can only be  
            // present in left subarray 
            if (bookList[mid].Id > target)
                return binarySearch(bookList, left, mid - 1, target);

            // Else the element  
            // can only be present 
            // in right subarray 
            return binarySearch(bookList, mid + 1, right, target);
        }

        // Returns list of elements in list that matches target  
        public static List<Book> BinarySearchByID(List<Book> bookList, double target)
        {
            bookList.Sort(); //Need to make sure incomming List<Book> is sorted before searching
            List<Book> matches = new List<Book>();

            int n = bookList.Count;
            int ind = binarySearch(bookList, 0, n - 1, target);

            // If element is not present 
            if (ind == -1)
                return matches;

            // Count/Add elements on left side if an element is present.             
            matches.Add(bookList[ind]);
            int left = ind - 1;
            while (left >= 0 && bookList[left].Id == target)
            {
                matches.Add(bookList[left]);
                left--;
            }

            // Count/Add elements on right side. 
            int right = ind + 1;
            while (right < n && bookList[right].Id == target)
            {
                matches.Add(bookList[right]);
                right++;
            }

            matches.Sort();
            return matches;
        }
    }
}
