﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjectA
{
    public class BookListNotFoundException : Exception
    {
        public BookListNotFoundException()
        {
        }

        public BookListNotFoundException(string message)
            : base(message)
        {
        }

        public BookListNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
