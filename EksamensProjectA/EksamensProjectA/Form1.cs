﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace EksamensProjectA
{
    public partial class MainForm : Form
    {
        private Controller control;
        private bool searchActivated = false;
        private delegate List<Book> SortDelegate();
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            List<Book> books;

            control = Controller.controllerInstance();
            try
            {
                books = control.LoadData();
                foreach (Book book in books)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = book.Id.ToString();
                    lvi.SubItems.Add(book.Title);
                    lv_Books.Items.Add(lvi);
                }

            }
            catch (Exception bex)
            {
                using (DialogCenteringService centeringService = new DialogCenteringService(this))
                {
                    MessageBox.Show($"Not possible to load data from source - Using default data\nError message: \n{ bex.Message }", "WARNING!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                books = control.LoadDefaultData();
                foreach (Book book in books)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = book.Id.ToString();
                    lvi.SubItems.Add(book.Title);
                    lv_Books.Items.Add(lvi);
                }
            }
        }

        private void existToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
        }

        private void loadBooklistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filePath;
            string fileName = "bookList.json";

            openFileDialog1.FileName = fileName;

            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filePath = openFileDialog1.FileName;
                string fileExtension = Path.GetExtension(filePath);
                if (fileExtension.Equals(".json"))
                {
                    using (DialogCenteringService centeringService = new DialogCenteringService(this)) // center message box
                    {
                        MessageBox.Show("Beklager - Indlæsning af egen datafil er endnu ikke implementeret", "OBS OBS !", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RadioButton sortChecked = null; //To check which RadioButton is checked/selected            
            if (((Button)sender).Text as string == "Sort") //If List is already sorted
            {                
                bool rbChecked = false;
                foreach (Control c in grpBoxSort.Controls)
                {
                    if (c is RadioButton)
                    {
                        RadioButton rb1 = c as RadioButton;
                        if (rb1.Checked == true)
                        {
                            sortChecked = rb1;
                            rb1.Checked = false;
                            rbChecked = true;
                            grpBoxSort.Enabled = false;
                        }
                    }
                }
                if (!rbChecked)
                {
                    using (DialogCenteringService centeringService = new DialogCenteringService(this)) // center message box
                    {
                        MessageBox.Show("Please select a sorting mechanism before sorting", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else //If list is unsorted
                {
                    ((Button)sender).Text = "Resort"; 
                    this.lv_Books.Items.Clear();
                    sortSelector(sortChecked); //Passing selected radion button to the sort selector to use the correct sort mechanism
                }
            }
            else //Reloading unsorted list
            {
                ((Button)sender).Text = "Sort";
                grpBoxSort.Enabled = true;
                this.reloadData();
            }
        }

        
       
        private void button2_Click(object sender, EventArgs e)
        {
            double searchID;
            if (!searchActivated)
            {
                if (Double.TryParse(txtBookID.Text, out searchID))
                {
                    searchID = Double.Parse(txtBookID.Text);
                    List<Book> matches = control.BinarySearchID(searchID);
                    this.loadSearchData(matches);                    
                    btnSearch.Text = "Clear Search";
                    searchActivated = true;
                    txtBookID.Enabled = false;
                    lvlBookID.Enabled = false;
                }
                else
                {
                    using (DialogCenteringService centeringService = new DialogCenteringService(this)) // center message box
                    {
                        MessageBox.Show("Please only use numeric numbers", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            else
            {
                txtBookID.Clear();
                this.lv_Books.Items.Clear();
                btnSearch.Text = "Search";
                searchActivated = false;
                txtBookID.Enabled = true;
                lvlBookID.Enabled = true;
                this.reloadData();
            }            
        }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
        {
            if (sender is RadioButton)
            {
                RadioButton rb1 = sender as RadioButton;
                if (rb1.Checked)
                {
                    //None implemented method to assign SortDelegate to correct sort mechanism in Controller/BooksList
                }
            }
        }

        private void chkBoxSimple_CheckedChanged(object sender, EventArgs e)
        {
            if(((CheckBox)sender).Checked == false)
            {
                grpBoxFastSearch.Enabled = false;
                txtBookIDSimple.Clear();
                txtBookTitleSimple.Clear();
                txtBookID.Enabled = true;
                lvlBookID.Enabled = true;
            }
            else
            {
                grpBoxFastSearch.Enabled = true;
                txtBookID.Enabled = false;
                lvlBookID.Enabled = false;
                txtBookID.Clear();
            }
        }

        private void btnClearAllSimple_Click(object sender, EventArgs e)
        {
            txtBookIDSimple.Clear();
            txtBookTitleSimple.Clear();
        }

        private void txtBookIDSimple_TextChanged(object sender, EventArgs e)
        {
            double searchID;
            if (txtBookIDSimple.Text.Length > 0)
            {
                txtBookTitleSimple.Enabled = false;
                this.lv_Books.Items.Clear();
                if(Double.TryParse(txtBookIDSimple.Text, out searchID))
                {
                    searchID = Double.Parse(txtBookIDSimple.Text);
                    List<Book> matches = control.SimpleSearchByID(searchID);
                    this.loadSearchData(matches);
                }
            }
            else
            {
                txtBookTitleSimple.Enabled = true;
                this.lv_Books.Items.Clear();
                this.reloadData();
            }                           
        }

        private void txtBookTitleSimple_TextChanged(object sender, EventArgs e)
        {
            string searchTitle;
            if (txtBookTitleSimple.Text.Length > 0)
            {
                txtBookIDSimple.Enabled = false;
                this.lv_Books.Items.Clear();
                searchTitle = txtBookTitleSimple.Text;
                List<Book> matches = control.SimpleSearchByTitle(searchTitle);
                this.loadSearchData(matches);
            }
            else
            {
                txtBookIDSimple.Enabled = true;
                this.lv_Books.Items.Clear();
                this.reloadData();
            }
        }

        private void txtBookID_TextChanged(object sender, EventArgs e)
        {
            if(txtBookID.Text.Length > 0 && !searchActivated)
            {
                grpBoxSort.Enabled = false;
                grpBoxSimpleActivate.Enabled = false;
                chkBoxSimple.Checked = false;
                btnSort.Enabled = false;
            }
            else
            {
                grpBoxSort.Enabled = true;
                grpBoxSimpleActivate.Enabled = true;
                btnSort.Enabled = true;
            }
            
        }

        #region HelperMethods
        private void reloadData()
        {
            this.lv_Books.Items.Clear();
            List<Book> books = control.GetOrgBookList();
            foreach (Book book in books)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = book.Id.ToString();
                lvi.SubItems.Add(book.Title);
                lv_Books.Items.Add(lvi);
            }
        }

        private void loadSearchData(List<Book> searchMatches)
        {
            this.lv_Books.Items.Clear();
            foreach (Book book in searchMatches)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = book.Id.ToString();
                lvi.SubItems.Add(book.Title);
                lv_Books.Items.Add(lvi);
            }
        }

        private void sortSelector(RadioButton rbChecked)
        {
            if (rbChecked.Text.ToLower().Contains("quick")) //Quick and dirty checker
            {
                SortDelegate sorter = control.QuickSort; //Assigning the correct sort mechanism from the controller class to the sort delegate
                Sort(sorter.Invoke()); //Invoking the SortDelegate to be passed to the sort method -> Returning List<Books>
            }
            else if (rbChecked.Text.ToLower().Contains("bubble"))
            {
                SortDelegate sorter = control.BubbleSort;
                Sort(sorter.Invoke());
            }
            else if (rbChecked.Text.ToLower().Contains("insert"))
            {
                SortDelegate sorter = control.InsertSort;
                Sort(sorter.Invoke());
            }
        }

        private void Sort(List<Book> bookList) //Clearing the ListView element and passing in a sorted List<Book>
        {
            this.lv_Books.Items.Clear();
            foreach (Book book in bookList)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = book.Id.ToString();
                lvi.SubItems.Add(book.Title);
                lv_Books.Items.Add(lvi);
            }
        }
        #endregion
    }
}
