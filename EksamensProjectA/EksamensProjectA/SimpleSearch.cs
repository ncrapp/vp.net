﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjectA
{
    public class SimpleSearch
    {
        //O(n)
        public static List<Book> SearchByID(List<Book> bookList, double target)
        {
            List<Book> match = bookList.FindAll((Book b) => { return b.Id == target; });
            return match;
        }
        //O(n)
        public static List<Book> SearchByTitle(List<Book> bookList, string target)
        {
            List<Book> match = bookList.FindAll((Book b) => { return b.Title.ToLower().Contains(target.ToLower()); });
            return match;
        }
        
        public static List<Book> Search(List<Book> bookList, string target)
        {
            List<Book> match;
            double targetID = 0;

            if(Double.TryParse(target, out targetID))
                match = bookList.FindAll((Book b) => { return b.Id == targetID; });          
            else
                match = bookList.FindAll((Book b) => { return b.Title.ToLower().Contains(target.ToLower()); });
            return match;
        }

        //O(n)
        public static List<Book> LiniarSearchByID(List<Book> bookList, double target)
        {
            List<Book> matches = new List<Book>();
            foreach(Book b in bookList)
            {
                if(b.Id == target)
                {
                    matches.Add(b);
                }
            }
            return matches;
        }
    }
}
