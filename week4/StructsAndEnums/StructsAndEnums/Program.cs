﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructsAndEnums
{
    class Program
    {
        enum Month
        {
            January, February, March, April,
            May, June, July, August,
            September, october, November, December
        }

        static void Main(string[] args)
        {
            doWork();
            Console.ReadLine();
        }

        static void doWork()
        {
            Month first = Month.December;
            Console.WriteLine(first);
            first++;
            Console.WriteLine(first);
        }
    }
}
