﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Dice
    {
        private int _eyes;
        private readonly Random _random;
        private int count;

        private static volatile Dice diceInstance;

        private Dice()
        {
            _random = new Random();
        }

        public static Dice DiceInstance
        {
            get
            {
                if(diceInstance == null)
                {
                    diceInstance = new Dice();
                }
                return diceInstance;
            }
        }

        public void ThrowDice()
        {
            _eyes = _random.Next(1, 7);
        }
        public int faceValue
        {
            get
            {
                return _eyes;
            }
        }
    }
}
