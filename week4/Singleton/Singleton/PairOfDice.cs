﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class PairOfDice
    {
        private Dice myDice;
        private int[] diceEyes;

        public PairOfDice()
        {
            myDice = Dice.DiceInstance;
            diceEyes = new int[2];
        }

        public void roll()
        {
            Array.Clear(diceEyes, 0, 2);

            Dice.DiceInstance.ThrowDice();
            diceEyes[0] = myDice.faceValue;
            Dice.DiceInstance.ThrowDice();
            diceEyes[1] = myDice.faceValue;
        }

        public int faceValue(int nr)
        {
            try
            {
                return diceEyes[nr-1];
            }
            catch
            {
                throw new IndexOutOfRangeException("Dice number is not valid");
            }
            
        }

        public int sum()
        {
            int sum = 0;
            for (int i = 0; i<diceEyes.Length; i++)
            {
                sum += diceEyes[i];
            }
            return sum;
        }

        public override string ToString()
        {
            string returnString = "\n";

            for(int i = 0; i<diceEyes.Length; i++)
            {
                int dieNo = i+1;
                
                returnString += "Dice " + dieNo + ": ";
                returnString += diceEyes[i];
                returnString += "\n";
            }
            return returnString;
        }

    }
}
