﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("*** WELCOME TO MiniMeyr ***");

            List<PairOfDice> pairOfDiceList = new List<PairOfDice>();

            bool displayMenu = true;
            while (displayMenu)
            {
                displayMenu = MainMenu(ref pairOfDiceList);
            }

            /*
            try
            {
                Console.WriteLine("Dice 1: " + dices.faceValue(1));                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Console.WriteLine("Dice 2: " + dices.faceValue(2));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine(dices.ToString());
            Console.WriteLine("Total value of dice eyes: " + dices.sum());
            */
            
            //Dice myDice = Dice.DiceInstance;
            //myDice.ThrowDice();
            Dice.DiceInstance.ThrowDice();
            Console.WriteLine(Dice.DiceInstance.faceValue);
            //Console.WriteLine(myDice.faceValue);

            Dice my2ndDice = Dice.DiceInstance;
            my2ndDice.ThrowDice();
            //Dice.DiceInstance.ThrowDice();
            Console.WriteLine(my2ndDice.faceValue);

            Dice my3rdDice = Dice.DiceInstance;
            my3rdDice.ThrowDice();
            //Dice.DiceInstance.ThrowDice();
            //Console.WriteLine(my3rdDice.faceValue);
            

            //Console.WriteLine("EXIT");
            Console.ReadLine();
        }
        /*

        private static void Roll(ref List<PairOfDice> pairOfDiceList)
        {
            PairOfDice dices = new PairOfDice();
            dices.roll();
            pairOfDiceList.Add(dices);
            Console.WriteLine("Roll result is: ");
            Console.WriteLine(dices.ToString());
            Console.WriteLine("Sum of dices is: " + dices.sum());
        }

        private static void Sixers(ref List<PairOfDice> pairOfDiceList)
        {
            int count = 0;

            foreach (PairOfDice dices in pairOfDiceList)
            {
                int value1 = dices.faceValue(1);
                int value2 = dices.faceValue(2);

                if(value1 == 6 && value2 == 6)
                {
                    count++;
                }
            }
            Console.WriteLine("Amount of dicepais in list: " + pairOfDiceList.Count);
            Console.WriteLine("Amount of 2 six'ers: " + count);
        }

        private static void Pair(ref List<PairOfDice> pairOfDiceList)
        {
            int count = 0;

            foreach (PairOfDice dices in pairOfDiceList)
            {
                int value1 = dices.faceValue(1);
                int value2 = dices.faceValue(2);

                if (value1 == value2)
                {
                    count++;
                }
            }
            Console.WriteLine("Amount of dicepairs in list: " + pairOfDiceList.Count);
            Console.WriteLine("Amount of 2-even: " + count);
        }

        private static void Meyer(ref List<PairOfDice> pairOfDiceList)
        {
            int count = 0;

            foreach (PairOfDice dices in pairOfDiceList)
            {
                int value1 = dices.faceValue(1);
                int value2 = dices.faceValue(2);

                if (value1 == 1 && value2 == 2)
                {
                    count++;
                }
                else if(value2 == 2 && value1 == 1)
                {
                    count++;
                }
            }
            Console.WriteLine("Amount of dicepairs in list: " + pairOfDiceList.Count);
            Console.WriteLine("Amount of Meyers: " + count);
        }

        private static bool MainMenu(ref List<PairOfDice> pairOfDiceList)
        {
            bool displayMenu = true;

            Console.WriteLine("Please enter choice:\n");
            Console.WriteLine("1) Roll new pair of dices");
            Console.WriteLine("2) How many six'ers");
            Console.WriteLine("3) How many pairs");
            Console.WriteLine("4) How many Meyers");            
            Console.WriteLine("0) Exit");

            int result;
            if (Int32.TryParse(Console.ReadLine(), out result))
            {
                switch (result)
                {
                    case 0:
                        displayMenu = false;
                        break;
                    case 1:
                        Roll(ref pairOfDiceList);
                        break;
                    case 2:
                        Sixers(ref pairOfDiceList);
                        break;
                    case 3:
                        Pair(ref pairOfDiceList);
                        break;
                    case 4:
                        Meyer(ref pairOfDiceList);
                        break;
                    default:
                        Console.WriteLine("\nPlease enter a valid option");
                        break;
                }
            }
            else
                Console.WriteLine("\nPlease enter a valid option");

            return displayMenu;
        }

        */
    }
}
