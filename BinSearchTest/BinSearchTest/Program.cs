﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinSearchTest
{
    class Program
    {
        static void Main(string[] args)
        {

            List<int> testList = new List<int>();

            int[] numbers = { 2, 4, 5, 5, 7, 7, 9 };
            

            Array.Sort(numbers);

            foreach(int number in numbers)
            {
                Console.Write(" {0}", number);
            }

            int[] indexes = binarySearchArrayWithDup(numbers, 3);

            Console.WriteLine("\n");
            try
            {
                for (int i = indexes[0]; i <= indexes[1]; i++)
                {
                    Console.Write(" {0}", numbers[i]);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ingen hits fundet: {0}", e.Message);
            }

            Console.ReadLine();
        }

        public static int doBinarySearchRec(int[] array, int start, int end, int n)
        {
            if (start > end)
            {
                return -1;
            }
            int mid = start + (end - start) / 2;

            if (n == array[mid])
            {
                return mid;
            }
            else if (n < array[mid])
            {
                return doBinarySearchRec(array, start, mid - 1, n);
            }
            else
            {
                return doBinarySearchRec(array, mid + 1, end, n);
            }
        }

        static int[] binarySearchArrayWithDup(int[] array, int n)
        {

            if (null == array)
            {
                return null;
            }
            int firstMatch = doBinarySearchRec(array, 0, array.Length - 1, n);
            int[] resultArray = { -1, -1 };
            if (firstMatch == -1)
            {
                return resultArray;
            }
            int leftMost = firstMatch;
            int rightMost = firstMatch;

            for (int result = doBinarySearchRec(array, 0, leftMost - 1, n); result != -1;)
            {
                leftMost = result;
                result = doBinarySearchRec(array, 0, leftMost - 1, n);
            }

            for (int result = doBinarySearchRec(array, rightMost + 1, array.Length - 1, n); result != -1;)
            {
                rightMost = result;
                result = doBinarySearchRec(array, rightMost + 1, array.Length - 1, n);
            }

            resultArray[0] = leftMost;
            resultArray[1] = rightMost;

            return resultArray;
        }
    }
}
