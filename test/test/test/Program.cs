﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Beer> beers = new List<Beer>();

            String name = "";
            int size = 0;
            int prize = 0;
            bool alcohol = false;
            double procent = 0.0;

            bool exit = false;

            while (!exit)
            {
                Console.WriteLine("Enter name of beer");
                name = Console.ReadLine();

                Console.WriteLine("Enter size of beer");
                bool sizeSet = false;
                while (!sizeSet)
                {
                    try
                    {
                        size = int.Parse(Console.ReadLine());
                        sizeSet = true;
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Input Error - Size needs to be an Integer");
                    }
                }

                Console.WriteLine("Enter prize of beer");
                bool prizetSet = false;
                while (!prizetSet)
                {
                    try
                    {
                        prize = int.Parse(Console.ReadLine());
                        prizetSet = true;
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Input Error - Enter prize as Integer");

                    }
                }

                bool confirmed = false;
                string choice;
                while (!confirmed)
                {
                    Console.WriteLine("Does the beer contains alcohol? (Y/N)");
                    try
                    {
                        choice = Console.ReadLine();
                        if (choice.ToLower() == "y" || choice.ToLower() == "n")
                        {
                            if (choice.Equals("y"))
                            {
                                alcohol = true;
                                Console.WriteLine("You selected: " + "'" + choice.ToUpper() + "'" + " The beer has alcohol? " + alcohol);
                                bool procentSet = false;
                                Console.WriteLine("Enter alcohol procent");

                                while (!procentSet)
                                {
                                    try
                                    {
                                        procent = double.Parse(Console.ReadLine());
                                        procentSet = true;
                                    }
                                    catch (FormatException)
                                    {
                                        Console.WriteLine("Input Error - Procent needs to be entered as a double - Eg. 4,7");
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }

                                }
                            }
                            else
                            {
                                alcohol = false;
                                procent = 0.0;
                                Console.WriteLine("You selected: " + "'" + choice.ToUpper() + "'" + " The beer has alcohol? " + alcohol);
                            }
                            confirmed = true;
                        }
                        else
                        {
                            throw new Exception();
                        }

                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Input Error - Please enter Y or N and press Enter");
                    }
                }

                beers.Add(new Beer(name, size, prize, alcohol, procent));
                Console.WriteLine("You have created a beer called: " + name);

                Console.WriteLine("\nWould you like to create another beer? [Y/N]");
                string saveChoice = Console.ReadLine();
                if (saveChoice.ToLower() == "n")
                    exit = true;
            }

            foreach (Beer beer in beers)
            {
                Console.WriteLine(beer.ToString());
            }
            Console.ReadLine();
        }
    }
}
