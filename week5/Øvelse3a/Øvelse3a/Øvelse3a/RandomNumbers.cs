﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Øvelse3a
{
    class RandomNumbers
    {
        private static Random rng = new Random();
        private int[] myArray;

        public RandomNumbers(int Count, int Min, int Max)
        {
            CreateArray(Count, Min, Max);
        }

        public RandomNumbers(int number)
        {
            int[] list = new int[15] {2, 2, 4, 5, 7, 7, 10, 19, 20, 20, 55, 60, 61, 62, 63 };
            Array.Sort(list);

            Console.WriteLine("Number to be inserted into sorted list: {0}\n", number);
            foreach (int no in list)
            {
                Console.Write("{0} ", no);
            }
            Console.WriteLine("");
            bool success = InsertElement(number, list);

            Console.WriteLine("\nWas {0} inserted in list ? {1}\n", number, success);
        }

        private void CreateArray(int count, int min, int max)
        {
            myArray = new int[count];
            for(int i = 0; i<myArray.Length; i++)
            {
                myArray[i] = rng.Next(min, max);
            }
        }

        public int[] getArray()
        {
            return myArray;
        }

        private int ReadInput(string text)
        {
            int x = Int32.Parse(text);
            return x;
        }

        public int CountNumbers(int number)
        {
            int count = 0;
            for(int i = 0; i<myArray.Length; i++)
            {
                if (myArray[i] == number)
                    count++;
            }
            return count;
        }

        public int CountNumbers2(int number)
        {
            int count = 0;
            Array.Sort(myArray);

            return count;
        }

        public void search(int[] inArray)
        {

        }

        bool InsertElement(int number, int[] list)
        {
            int pos = 0;
            bool result = false;

            for (int i = 1; i < list.Length - 1; i++)
            {
                if (list[i - 1] <= number && number <= list[i])
                {
                    pos = i;
                    for (int k = list.Length - 1; k > pos; k--)
                    {
                        list[k] = list[k - 1];
                    }
                    result = true;
                }
                else
                {
                    pos = list.Length - 1;
                    result = true;
                }
            }
            list[pos] = number;
            Console.WriteLine("Position: {0}", pos);

            foreach (int nummer in list)
            {
                Console.Write("{0} ", nummer);
            }

            for (int i = 0; i < list.Length; i++)
            {
                //Console.Write("{0} ", list[i]);
            }

            return result;
        }
    }

}
