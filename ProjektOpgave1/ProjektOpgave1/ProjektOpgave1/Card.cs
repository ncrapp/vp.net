﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOpgave1
{
    public enum Suit { Spades, Clubs, Hearts, Diamonds}
    public enum Value { Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King}

    public class Card : IComparable<Card>
    {
        private Value cardValue;
        private Suit cardSuit;        

        public Card(int value, int suit)
        {
            cardValue = (Value)value;
            cardSuit = (Suit)suit;
        }

        public Suit Suit()
        {
            return cardSuit;
        }

        public Value Value()
        {
            return cardValue;
        }

        public override string ToString()
        {
            string returnString = "This card is: ";
            returnString += Value().ToString();
            returnString += " of ";
            returnString += Suit().ToString();
            returnString += "\n";
            return returnString;
        }

        public int CompareTo(Card other)
        {
            if(this.cardSuit > other.cardSuit)
            {
                return 1;
            }
            else if(this.cardSuit < other.cardSuit)
            {
                return -1;
            }
            else //if CarSuits are equal, test for value
            {
                if(this.cardValue > other.cardValue)
                {
                    return 1;
                }
                else if(this.cardValue < other.cardValue)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
