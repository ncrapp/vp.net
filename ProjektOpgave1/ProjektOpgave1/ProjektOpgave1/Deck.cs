﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOpgave1
{
    class Deck
    {
        private Card[] cards;

        private int amountofCardsPerSuit;
        private int amountOfSuits;
        private int amountOfCards;    

        public Deck()
        {
            amountofCardsPerSuit = 13;
            amountOfSuits = 4;
            amountOfCards = amountofCardsPerSuit * amountOfSuits;
            cards = new Card[amountOfCards];

            int startofSuit = 0;
            int startofValue = 0;

            for(int i = 0; i < amountOfCards; i++)
            {
                cards[i] = new Card(startofValue, startofSuit);

                if(startofSuit <= amountOfSuits - 1)
                {
                    if(startofValue < amountofCardsPerSuit -1)
                    {
                        startofValue++;
                    }
                    else
                    {
                        startofValue = 0;
                        if(startofSuit < amountOfSuits - 1)
                        {
                            startofSuit++;
                        }
                    }
                }
            }
            Console.WriteLine("Sorted Deck: ");
            for (int i = 0; i<cards.Length; i++)
            {
                Console.WriteLine(cards[i].ToString());
            }
            Console.WriteLine("\n");

            Shuffler.Shuffle(cards);

            Console.WriteLine("Shuffled Deck: ");
            for (int i = 0; i < cards.Length; i++)
            {
                Console.WriteLine(cards[i].ToString());
            }

            Console.WriteLine("\n");
            
        }

        public void Sort()
        {
            Shuffler.Sort(cards);
        }
    }
}
