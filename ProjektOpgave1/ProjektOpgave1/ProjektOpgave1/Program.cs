﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOpgave1
{
    class Program
    {
        private static Deck deck;

        static void Main(string[] args)
        {
            bool displayMenu = true;
            while (displayMenu)
            {
                displayMenu = MainMenu();
            }

            Console.ReadLine();
        }


        static bool MainMenu()
        {
            bool displayMenu = true;

            Console.WriteLine("Please enter choice:\n");
            Console.WriteLine("1) Shuffle Cards");
            Console.WriteLine("2) Sort Cards");
            Console.WriteLine("0) Exit and re-sort Deck");

            int result;
            if (Int32.TryParse(Console.ReadLine(), out result))
            {
                switch (result)
                {
                    case 0:
                        Console.WriteLine("Exiting and Sorting Deck");
                        displayMenu = false;                        
                        break;
                    case 1:
                        deck = new Deck();                        
                        break;
                    case 2:
                        try
                        {
                            deck.Sort();
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("Please create new deck and shuffle before sorting");
                        }                                                        
                        break;
                    default:
                        Console.WriteLine("\nPlease enter a valid option");
                        break;
                }
            }
            else
                Console.WriteLine("\nPlease enter a valid option");

            return displayMenu;
        }
    }
}
