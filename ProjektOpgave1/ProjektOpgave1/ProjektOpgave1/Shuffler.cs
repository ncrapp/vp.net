﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOpgave1
{
    static class Shuffler
    {
        private static Random rng = new Random();

        public static void Shuffle(Card[] deck)
        {
            int n = deck.Length;            

            while(n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                Card c = deck[k];
                deck[k] = deck[n];
                deck[n] = c;
            }
        }

        public static void Sort(Card[] deck)
        {
            Array.Sort(deck);

            Console.WriteLine("Re-Sorted Deck: ");
            for (int i = 0; i < deck.Length; i++)
            {
                Console.WriteLine(deck[i].ToString());
            }
        }
    }
}
