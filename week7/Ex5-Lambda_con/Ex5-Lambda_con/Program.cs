﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex5_Lambda_con
{
    class Program
    {
        delegate bool IsAdult(Student stud);

        static void Main(string[] args)
        {
            IsAdult isAdult = (s) => {
                int adultAge = 18;
                Console.WriteLine("Lambda expression with multiple statements in the body");
                return s.Age >= adultAge;
                };
            Student stud = new Student() { Age = 25 };
            Console.WriteLine(isAdult(stud));

            Console.ReadLine();
        }
    }

    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
