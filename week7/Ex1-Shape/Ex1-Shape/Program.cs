﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1_Shape
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape s1 = new Shape(5, 7);
            Rectangle r1 = new Rectangle(3, 6);
            Triangle t1 = new Triangle(40, 76);

            Console.WriteLine(r1.Area());
            Console.WriteLine(t1.Area());

            s1.Display();
            r1.Display();
            t1.Display();

            Console.ReadLine();
        }
    }
}
