﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1_Shape
{
    class Rectangle : Shape
    {
        public Rectangle(int width, int height) : base(width, height)
        {
        }

        public override double Area()
        {
            return width * height;
        }

        public override void Display()
        {
            Console.WriteLine("I am a Rectangle");
        }
    }
}
