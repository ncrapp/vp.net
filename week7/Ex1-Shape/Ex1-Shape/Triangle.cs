﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1_Shape
{
    class Triangle : Shape
    {
        public Triangle(int width, int height) : base(width, height)
        {
        }

        public override double Area()
        {
            return 0.5 * height * width;
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override void Display()
        {
            Console.WriteLine("I am a Triangle - I am also a: ");
            base.Display();
        }

    }
}
