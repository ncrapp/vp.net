﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1_Shape
{
    class Shape
    {
        protected int width;
        protected int height;

        public Shape(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        public virtual double Area()
        {
            return 0;
        }

        public virtual void Display()
        {
            Console.WriteLine("I am a Shape");
        }

        public int Width { get => width; set => width = value; }
        public int Height { get => height; set => height = value; }
    }
}
