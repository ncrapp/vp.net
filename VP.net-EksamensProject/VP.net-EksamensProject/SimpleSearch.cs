﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VP.net_EksamensProject
{
    class SimpleSearch
    {
        public static List<Book> SearchByID(List<Book> bookList, double target)
        {
            List<Book> match = bookList.FindAll((Book b) => { return b.Id == target; });
            return match;
        }

        public static List<Book> SearchByTitle(List<Book> bookList, string target)
        {
            List<Book> match = bookList.FindAll((Book b) => { return b.Title.Contains(target); });
            return match;
        }

        public static List<Book> Search(List<Book> bookList, string target)
        {
            List<Book> match;
            double targetID = 0;

            if(Double.TryParse(target, out targetID))
                match = bookList.FindAll((Book b) => { return b.Id == targetID; });          
            else
                match = bookList.FindAll((Book b) => { return b.Title.Contains(target); });
            return match;
        }
    }
}
