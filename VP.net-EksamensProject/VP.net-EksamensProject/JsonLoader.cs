﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace VP.net_EksamensProject
{
    class JsonLoader : AbstractDataLoader
    {
        private string jsonResult;
        private string filePath;
        private List<Book> bookList;

        public JsonLoader(string filePath)
        {
            bookList = new List<Book>();
            this.filePath = filePath;
        }

        public override List<Book> Load()
        {
            if (File.Exists(filePath))
            {                
                using (StreamReader reader = new StreamReader (filePath, true))
                {
                    jsonResult = reader.ReadToEnd();                    
                    reader.Close();
                }
            }

            bookList = JsonConvert.DeserializeObject<List<Book>>(jsonResult);
            return bookList;
        }

        public override void Save(List<Book> booksSave)
        {
            jsonResult = JsonConvert.SerializeObject(booksSave, Formatting.Indented);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                using (var tw = new StreamWriter(filePath, true))
                {
                    tw.WriteLine(jsonResult.ToString());
                    tw.Close();
                }
            }
            else if (!File.Exists(filePath))
            {
                using (var tw = new StreamWriter(filePath, true))
                {
                    tw.WriteLine(jsonResult.ToString());
                    tw.Close();
                }
            }
        }
    }
}
