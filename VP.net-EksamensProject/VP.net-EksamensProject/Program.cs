﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace VP.net_EksamensProject
{
    class Program
    {        
        static void Main(string[] args)
        {
            BookList bookList = null;
            
            List<Book> books = new List<Book>();

            string dir = Directory.GetCurrentDirectory();
            string filePath = dir + @"\" + "bookList.json";
            IDataLoader dataLoader = new JsonLoader(filePath);

            //bookList = BookList.Instance(dataLoader.Load());
            bookList = BookList.Instance();
            bookList.SetBookList(dataLoader.Load());
            Console.WriteLine("Books in booklist:\n");
            
            /*
            Book bookA = new Book(79.41, "Rasmus Klump i Pingonesien");
            Book bookB = new Book(50, "NaturTeknikfager.dk");
            Book bookC = new Book(99.4, "The Hobbits - the many lives of Bilbo, Sam, Merry and Pippin");
            Book bookD = new Book(50.264, "How to BinarySearch");
            Book bookE = new Book(79.41, "Broken Dimensions");
            Book bookF = new Book(50.264, "Fremtidens natur i Sønderborg Kommune");
            Book bookG = new Book(37.8, "Kunst af lyst");
            Book bookH = new Book(50.264, "By, sø, skov");
            Book bookI = new Book(15.14, "Trying jsonLoad");

            bookList.AddBook(bookA);            
            bookList.AddBook(bookB);
            bookList.AddBook(bookC);
            bookList.AddBook(bookD);
            bookList.AddBook(bookE);
            bookList.AddBook(bookF);
            bookList.AddBook(bookG);
            bookList.AddBook(bookH);
            bookList.AddBook(bookI);
            */

            //Console.WriteLine(books.ToString());

            books = bookList.getBooks();
            //List<Book> books = bookList.QuickSort(bookList.getBooks());            
            //bookList.BubbleSort(books);
            //bookList.InsertSort(books);

            //dataLoader.Save(books);

            foreach (Book b in books)
            {
               Console.WriteLine(b.ToString());
            }

            Console.WriteLine("\n");
            books = bookList.QuickSort(bookList.getBooks());

            foreach (Book b in books)
            {
                Console.WriteLine(b.ToString());
            }

            Console.WriteLine("\n");
            bookList.AddBook(new Book(55.55, "testbog"));

            foreach (Book b2 in bookList.getBooks())
            {
                Console.WriteLine(b2.ToString());
            }

            List<Book> matches = BinarySearcher.BinarySearchByID(books, 15.14);

            Console.WriteLine("\n Books found:");
            foreach(Book b in matches)
            {
                Console.WriteLine("ID: {0} - Title: {1}", b.Id, b.Title);
            }
            Console.WriteLine("\n");

            List<Book> match = SimpleSearch.Search(books, "50");
            foreach (Book b in match)
            {
                Console.WriteLine(b);
            }
            
            Console.ReadLine();
        }
    }
}
