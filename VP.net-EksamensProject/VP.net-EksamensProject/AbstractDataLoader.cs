﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VP.net_EksamensProject
{
    public abstract class AbstractDataLoader : IDataLoader
    {
        private string path;
        private string credentials;
        private List<Book> bookList;

        public AbstractDataLoader() : this("", "")
        {
            bookList = new List<Book>();
        }

        public AbstractDataLoader(string path) : this(path, "")
        {
            bookList = new List<Book>();
        }

        public AbstractDataLoader(string path, string credentials)
        {
            bookList = new List<Book>();
            this.path = path;
            this.credentials = credentials;
        }

        public List<Book> BookList { set; get; }
        public string Path { set; get; }
        public string Credentials { set; get; }

        public virtual List<Book> Load()
        {
            return bookList;
        }

        public virtual void Save(List<Book> books)
        {
        }

    }
}
