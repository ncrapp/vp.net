﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beers2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Beer> beers = new List<Beer>();

            bool exit = false;

            String name = "";
            int size = 0;
            int prize = 0;
            bool alcohol = false;
            double procent = 0.0;

            int menuPkt = 1;
            switch (menuPkt)
            {
                case 1:
                    Console.WriteLine("Enter name of beer");
                    name = Console.ReadLine();

                    Console.WriteLine("Enter size of beer");
                    bool sizeSet = false;
                    while (!sizeSet)
                    {
                        try
                        {
                            size = int.Parse(Console.ReadLine());
                            sizeSet = true;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Input Error - Size needs to be an Integer");
                        }
                    }

                    Console.WriteLine("Enter prize of beer");
                    bool prizetSet = false;
                    while (!prizetSet)
                    {
                        try
                        {
                            prize = int.Parse(Console.ReadLine());
                            prizetSet = true;
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Input Error - Enter prize as Integer");

                        }
                    }

                    bool confirmed = false;
                    string choice;
                    while (!confirmed)
                    {
                        Console.WriteLine("Does the beer contains alcohol? (Y/N)");
                        try
                        {
                            choice = Console.ReadLine();
                            if (choice.ToLower() == "y" || choice.ToLower() == "n")
                            {
                                if (choice.Equals("y"))
                                {
                                    alcohol = true;
                                    Console.WriteLine("You selected: " + "'" + choice.ToUpper() + "'" + " The beer has alcohol? " + alcohol);
                                    bool procentSet = false;
                                    Console.WriteLine("Enter alcohol procent");

                                    while (!procentSet)
                                    {
                                        try
                                        {
                                            procent = double.Parse(Console.ReadLine());                                            
                                            procentSet = true;
                                        }
                                        catch (FormatException)
                                        {
                                            Console.WriteLine("Input Error - Procent needs to be entered as a double - Eg. 4,7");
                                        }
                                        catch (Exception e)
                                        {
                                            Console.WriteLine(e.Message);
                                        }

                                    }
                                }
                                else
                                {
                                    alcohol = false;
                                    procent = 0.0;
                                    Console.WriteLine("You selected: " + "'" + choice.ToUpper() + "'" + " The beer has alcohol? " + alcohol);
                                }
                                confirmed = true;
                            }
                            else
                            {
                                throw new Exception();
                            }

                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Input Error - Please enter Y or N and press Enter");
                        }
                    }

                    beers.Add(new Beer(name, size, prize, alcohol, procent));
                    Console.WriteLine("You have created a beer called: " + name);
                    goto case 2;
                case 2:                    
                    string saveChoice;

                    Console.WriteLine("\nWould you like to create another beer? [Y/N]");
                    saveChoice = Console.ReadLine();
                    if (saveChoice.ToLower() == "y")
                    {
                        goto case 1;
                    }
                    else if (saveChoice.ToLower() == "n")
                    {
                        goto case 3;
                    }
                    else Console.WriteLine("Input error - Please enter Y or N");
                        goto case 2;
                case 3:
                    Console.WriteLine("\nYou have added following beers to the list\n");
                    foreach (Beer beer in beers)
                    {
                        Console.WriteLine(beer.ToString());
                    }                    
                    break;
                default:                    
                    break;
            }

            Console.ReadLine();

            /*
            Beer b = new Beer("Tuborg", 33, 5, true, 5.0);
            Console.WriteLine(b.ToString());
            Console.WriteLine("Prize is : " + b.getPrize());
            Console.ReadLine();
            */
        }
    }
}
