﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beers2
{
    class Beer
    {
        #region fields and properties
        private String name;
        private int size;
        private int prize;
        private bool alcohol;
        private double procent;
        #endregion

        public double Procent
        {
            get { return procent; }
            set {

                try
                {
                    if (value >= 0.0 && value < 100.0)
                    {
                        procent = value;
                    }
                    else
                        throw new Exception("Exception: Procent needs to be between 0.0 and 100.0");
                }
                
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw e;
                }

            }
        }
        public bool Alcohol
        {
            get { return alcohol; }
            set { alcohol = value; }
        }
        public String Name
        {
            get { return name; }
            set {
                    try
                    {
                    if (value != "")
                        name = value;
                    else
                        throw new Exception();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine ("Name cannot be blank");
                    }
                }
        }
        public int Size
        {
            get { return size; }
            set {
                    try
                    {
                        if (value > 0)
                        {
                            size = value;
                        }
                        else
                            throw new Exception();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Size needs to be positive");
                    }
                }
        }
        public int Prize
        {
            get { return prize; }
            set {
                try
                {
                    if (value > 0)
                    {
                        prize = value;
                    }
                    else
                        throw new Exception();
                }
                catch (Exception)
                {
                    Console.WriteLine("Prize needs to be positive");
                }

            }
        }

        public Beer(string name, int size, int prize)
        {
            this.Name = name;
            this.Size = size;
            this.Prize = prize;
        }

        public Beer(string name, int size, int prize, bool alcohol, double procent)
        {
            this.Name = name;
            this.Size = size;
            this.Prize = prize;
            this.Alcohol = alcohol;
            this.Procent = procent;
        }

        public int getPrize()
        {
            return prize;
        }

        public override string ToString()
        {
            return
                "Name; " + this.name +
                "\nSize: " + this.Size +
                "\nPrice: " + this.Prize +
                "\nAlcohol: " + this.Alcohol +
                "\nProcent: " + this.Procent +
                "\n";
        }
    }
}


