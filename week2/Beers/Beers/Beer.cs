﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beers
{
    class Beer
    {
        #region fields and properties
        private String name;
        private int size;
        private int prize;

        public string Name { get => name; set => name = value; }
        public int Size { get => size; set => size = value; }
        public int Prize { get => prize; set => prize = value; }
        #endregion

        public Beer(string name, int size, int prize)
        {
            this.Name = name;
            this.size = size;
            this.Prize = prize;
        }

        public int getPrize()
        {
            return prize;
        }

        public override string ToString()
        {
            return "I am a " + this.name + " beer";
        }
    }
}


