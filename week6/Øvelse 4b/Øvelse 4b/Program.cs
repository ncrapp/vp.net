﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Øvelse_4b
{
    class Program
    {
        //static List<string> elever = new List<string><string>();
        static Queue<string> elever = new Queue<string>();
        static bool run = true;
        static int count = 0;
        static int aNameCount = 0;

        static void Main(string[] args)
        {
            while (run)
            {
                string input;
                Console.WriteLine("Indtast et navn ('Stop' for at afslutte");
                input = Console.ReadLine();
                if (input.ToLower() == "stop")
                {
                    run = false;
                    Console.WriteLine("afslutter program");
                }
                else
                {
                    elever.Enqueue(input);
                    count++;
                }
            }
            Console.WriteLine("Antal indtastede elever via count: {0}", count);
            Console.WriteLine("Antal indtastede elever via list count: {0}", elever.Count());
            Console.WriteLine("De indtastede navne er følgende: ");
            foreach (string navn in elever)
            {
                Console.WriteLine(navn);
            }
            foreach (string aNavn in elever)
            {
                if (aNavn[0].ToString().ToLower() == "a")
                {
                    aNameCount++;
                }
            }
            Console.WriteLine("Antal fornavne, der starter med \"A\": {0} ", aNameCount);
            string allNames = string.Join("", elever);
            double avgChars = ((Double)allNames.Length / elever.Count);
            Console.WriteLine("Gennemsnitslængde af fornavne: {0}" ,avgChars);

            Console.ReadLine();
        }

    }
}
