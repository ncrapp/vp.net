﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Grenade_Frag : AbstractGrenade
    {
        public Grenade_Frag() : this(300, "Frag Grenade", 1)
        {
        }

        public Grenade_Frag(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            effectRadius = 0.5;
            effectDuration = 5;
            effectDamage = 90;
        }

        public override int DoDamage()
        {
            Console.WriteLine($"You throw your {Name}");
            DoEffect();
            return effectDamage;
        }

        public override void DoEffect()
        {
            Console.WriteLine($"Your {Name} lands and does {effectDamage} to all enemies in a radius of {effectRadius}m and confuses them for {effectDuration}s");
        }
    }
}
