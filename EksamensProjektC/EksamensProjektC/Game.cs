﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Game
    {
        private bool endProgram = false;
        private bool endGame = false;

        public Game()
        {
            Console.WriteLine("Welcome to Simplified Unit game");
            Console.WriteLine("-------------------------------\n");
            MainMenu();

        }

        private void MainMenu()
        {
            endGame = false;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Please select a Unit from below list - Press escape to exit program\n");
            Console.WriteLine("1.... Marine  - Heavy infantry soldier with 50% amplified damage");
            Console.WriteLine("2.... Soldier - Light and faster soldier with 20% amplified damage");
            Console.WriteLine("\n");
            while (!endProgram)
            {
                ConsoleKey keyInput = Console.ReadKey(true).Key;

                switch (keyInput)
                {
                    case ConsoleKey.Escape:
                        Console.WriteLine("Ending program.........");
                        endProgram = true;
                        Environment.Exit(0);
                        break;
                    case ConsoleKey.D1:
                        Console.WriteLine("You have selected the Heavy Infantry Marine");
                        AbstractUnit unit_marine = new Unit_Marine("Marine", 0.50);
                        UnitBuilder marine_builder = new MarineBuilder(unit_marine);
                        UnitDirector director = new MarineDirector();
                        IUnit u = director.Build(marine_builder);
                        runGame(unit_marine);
                        break;
                    case ConsoleKey.D2:
                        Console.WriteLine("You have selected the Light Sniper Soldier");
                        AbstractUnit unit_soldier = new Unit_Soldier("Soldier", 0.20);
                        UnitBuilder soldier_builder = new SoldierBuilder(unit_soldier);
                        UnitDirector soldier_director = new SoldierDirector();
                        IUnit u2 = soldier_director.Build(soldier_builder);
                        runGame(unit_soldier);
                        break;
                    default:
                        break;
                }
            }
        }

        private void runGame(AbstractUnit unit)
        {
            Console.WriteLine("Following options are available for playing the game");
            Console.WriteLine("Move forward             : W");
            Console.WriteLine("Move backwards           : S");
            Console.WriteLine("Strafe left              : A");
            Console.WriteLine("Strafe Right             : D");
            Console.WriteLine("Shoot                    : Enter");
            Console.WriteLine("Reload                   : R");
            Console.WriteLine("Toggle secondary ability : Spacebar");
            Console.WriteLine("Select Previous Weapon   : Left-Arrow");
            Console.WriteLine("Select Next Weapon       : Right-Arrow");
            Console.WriteLine("Simulate 'Take Damage'   : K");
            Console.WriteLine("End game                 : Escape");
            Console.WriteLine("\n");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Starting game....");
            Console.WriteLine("Your Unit stands infront of an enemy - Now it is time for actions....");
            while (!endGame && !endGame)
            {
                ConsoleKey keyInput = Console.ReadKey(true).Key;

                if (unit.IsAlive)
                {
                    switch (keyInput)
                    {
                        case ConsoleKey.Escape:
                            Console.WriteLine("\nEnding game.........\n");
                            endGame = true;
                            MainMenu();
                            break;
                        case ConsoleKey.W:
                            unit.moveForward();
                            break;
                        case ConsoleKey.S:
                            unit.moveBackwards();
                            break;
                        case ConsoleKey.A:
                            unit.moveLeft();
                            break;
                        case ConsoleKey.D:
                            unit.moveRight();
                            break;
                        case ConsoleKey.Spacebar:
                            unit.ToggleSecondary();
                            break;
                        case ConsoleKey.Enter:
                            unit.Attack();
                            break;
                        case ConsoleKey.LeftArrow:
                            unit.switchPrevWeapon();
                            break;
                        case ConsoleKey.RightArrow:
                            unit.switchNextWeapon();
                            break;
                        case ConsoleKey.R:
                            unit.ReloadWeapon();
                            break;
                        case ConsoleKey.K:
                            unit.TakeDamage(30);
                            break;
                        case ConsoleKey.P:
                            Console.WriteLine("\n");
                            Console.WriteLine($"{unit.ToString()} and has following weapons equiped");
                            foreach (AbstractWeapon weapon in unit.WeaponSlots)
                            {
                                Console.WriteLine(weapon.ToString());
                            }
                            Console.WriteLine("\n");
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("You died... End game by pressing Escape");
                    switch (keyInput)
                    {
                        case ConsoleKey.Escape:
                            Console.WriteLine("\nEnding game.........\n");
                            endGame = true;
                            MainMenu();
                            break;
                        default:
                            break;
                    }
                }
            }

        }
    }
}
