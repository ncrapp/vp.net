﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractWeapon : IWeapon
    {
        private int price;
        private string name;
        protected int baseDamage; //Subclasses needs to be able to set this variable in constructorbudy (eg. Rifle_M4A4)

        public AbstractWeapon(int price, string name, int baseDamage)
        {
            this.price = price;
            this.name = name;
            this.baseDamage = baseDamage;
        }

        public int Price { get => price;  }
        public string Name { get => name; }
        public int BaseDamage { get => baseDamage;}

        public abstract int DoDamage(); //Denne metode kunne laves virtual med en top implementering, men det er mere generelt at et weapon implementerer denne end det ikke gør (grenades)

        public override string ToString()
        {
            string returnString = $"Your Weapon is a {this.GetType().Name} called {name} - Price is {price} and it's basedamage is {BaseDamage}";
            return returnString;
        }
    }
}
