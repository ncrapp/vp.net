﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Gun_Usp : AbstractGun, ISecondarable
    {
        private bool secondaryEffect = false;
        private string effectName = "silencer";

        public Gun_Usp(int price, string name) : base(price, name)
        {
            baseDamage = 35;
            bulletCapacity = 24;
            magCapacity = 12;
            magSize = magCapacity;
            roundsPerMinut = 352;
            hitRange = 21;
            recoilLevel = 0.69;
            noiseLevel = 553;
            reloadTime = 2.5;
        }

        public Gun_Usp(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            bulletCapacity = 24;
            magCapacity = 12;
            magSize = magCapacity;
            roundsPerMinut = 352;
            hitRange = 21;
            recoilLevel = 0.69;
            noiseLevel = 553;
            reloadTime = 2.5;
        }

        public Gun_Usp(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : base(price, name, baseDamage, bulletCapacity, magCapacity, rmp, hitRange, recoilLevel, noiseLevel, reloadTime)
        {
        }

        public bool SecondaryEffect { get => secondaryEffect; }
        public string EffectName { get => effectName; }

        public void ToggleSecondaryEffect()
        {
            if (!secondaryEffect)
            {
                Console.WriteLine($"Enabling {effectName} on {Name}");
                noiseLevel = noiseLevel / 2;
                secondaryEffect = true;
                baseDamage = baseDamage / 2;
            }
            else
            {
                Console.WriteLine($"Disabling {effectName} on {Name}");
                noiseLevel = 553;
                secondaryEffect = false;
                baseDamage = 35;
            }

        }
    }
}
