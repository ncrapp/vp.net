﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractGrenade : AbstractWeapon
    {
        protected double effectRadius; //Area of effect in meters
        protected double effectDuration;   //Duration of effect in seconds
        protected int effectDamage; //Damage for when grenade explodes

        public AbstractGrenade(int price, string name) : this(price, name, 1)
        {
        }

        public AbstractGrenade(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
        }

        public abstract void DoEffect();

        public override string ToString()
        {
            string returnString = $"Your grenade is {Name} and throw damage is {baseDamage} \nEffect area is {effectRadius}meters and last for {effectDuration}seconds";
            return returnString;
        }
    }
}
