﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    class DisplayMenu
    {
        private bool run = true;

        public DisplayMenu()
        {
            MainMenu();
        }

        private void MainMenu()
        {
            while (run)
            {
                Console.WriteLine("Enter input");
                runGame();
                Console.ReadLine();
            }
            
        }

        private void runGame()
        {
            Console.WriteLine("Running Game");
        }
    }
}
