﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Unit_Marine : AbstractUnit
    {
        public Unit_Marine(string unitName, double damageAmp) : base(unitName, damageAmp)
        {
        }

        public Unit_Marine(string unitName, double damageAmp, int maxGrenade, int maxShootingWeapon) : base(unitName, damageAmp, maxGrenade, maxShootingWeapon)
        {
        }
    }
}
