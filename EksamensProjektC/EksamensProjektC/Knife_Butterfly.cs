﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Knife_Butterfly : AbstractKnife
    {
        private bool secondaryEffect = false;
        private string effectName = "backstab";

        public Knife_Butterfly(string name) : base(name)
        {
        }

        public Knife_Butterfly(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
        }

        public override bool SecondaryEffect { get => secondaryEffect; }
        public override string EffectName { get => effectName; }

        public override int DoDamage()
        {
            if (!secondaryEffect)
            {
                Console.WriteLine($"Your {Name} slashes with normal cut for {baseDamage}");
                return baseDamage;
            }
            else
            {
                Console.WriteLine($"Your {Name} stabs with a backstab for {baseDamage}");
                return baseDamage;
            }
        }

        public override void ToggleSecondaryEffect()
        {
            if (!secondaryEffect)
            {
                Console.WriteLine($"Enabling {effectName} on {Name}");
                baseDamage += baseDamage/2;
                secondaryEffect = true;
            }
            else
            {
                Console.WriteLine($"Disabling {effectName} on {Name}");
                baseDamage = 34;
                secondaryEffect = false;
            }
        }
    }
}
