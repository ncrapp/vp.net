﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractKnife : AbstractWeapon, ISecondarable
    {
        public AbstractKnife(string name) : this(0, name, 34)
        {
        }

        public AbstractKnife(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
        }

        public abstract bool SecondaryEffect { get; }
        public abstract string EffectName { get; }
        public abstract void ToggleSecondaryEffect();
    }
}
