﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Gun_Glock : AbstractGun, ISecondarable
    {
        private bool secondaryEffect = false;
        private string effectName = "automatic";

        public Gun_Glock(int price, string name) : base(price, name)
        {
            baseDamage = 30;
            bulletCapacity = 120;
            magCapacity = 20;
            magSize = magCapacity;
            roundsPerMinut = 400;
            hitRange = 20.5;
            recoilLevel = 0.84;
            noiseLevel = 475;
            reloadTime = 2.27;
        }

        public Gun_Glock(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            bulletCapacity = 120;
            magCapacity = 20;
            magSize = magCapacity;
            roundsPerMinut = 400;
            hitRange = 20.5;
            recoilLevel = 0.84;
            noiseLevel = 475;
            reloadTime = 2.27;
        }

        public Gun_Glock(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : base(price, name, baseDamage, bulletCapacity, magCapacity, rmp, hitRange, recoilLevel, noiseLevel, reloadTime)
        {
        }

        public bool SecondaryEffect { get => secondaryEffect; }
        public string EffectName { get => effectName; }

        public void ToggleSecondaryEffect()
        {
            if (!secondaryEffect)
            {
                Console.WriteLine($"Enabling {effectName} on {Name}");
                roundsPerMinut = 1200;
                hitRange = 7.4;
                secondaryEffect = true;
            }
            else
            {
                Console.WriteLine($"Disabling {effectName} on {Name}");
                roundsPerMinut = 400;
                hitRange = 20.5;
                secondaryEffect = false;
            }

        }
    }
}
