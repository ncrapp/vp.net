﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractShootingWeapon : AbstractWeapon
    {
        protected int bulletCapacity;
        protected int magCapacity;
        protected int magSize;
        protected int roundsPerMinut;
        protected double hitRange;
        protected double recoilLevel;
        protected double noiseLevel;
        protected double reloadTime;

        public int BulletCapacity => bulletCapacity;
        public int MagCapacity => magCapacity;
        public int MagSize => magSize;
        public int RoundsPerMinut => roundsPerMinut;
        public double HitRange => hitRange;
        public double RecoilLevel => recoilLevel;
        public double NoiseLevel => noiseLevel;
        public double ReloadTime => reloadTime;

        public AbstractShootingWeapon(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
        }

        public AbstractShootingWeapon(int price, string name) : this(price, name, 0)
        {
        }

        public AbstractShootingWeapon(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : this(price, name, baseDamage)
        {
            this.bulletCapacity = bulletCapacity;
            this.magCapacity = magCapacity;
            this.magSize = magCapacity;
            this.roundsPerMinut = rmp;
            this.hitRange = hitRange;
            this.recoilLevel = recoilLevel;
            this.noiseLevel = noiseLevel;
            this.reloadTime = reloadTime;
        }

        public void Reload()
        {
            int freeMagSpace = magSize - magCapacity;
            if (magCapacity == magSize)
            {
                Console.WriteLine($"Reloading {Name} - No need to reload - magasine already full");
            }
            else
            {

                if (bulletCapacity >= freeMagSpace)
                {
                    magCapacity += freeMagSpace;
                    bulletCapacity -= freeMagSpace;
                    Console.WriteLine($"Reloading {Name} - You have {magCapacity} bullets in magasine champer and {bulletCapacity} left outside champer");
                }
                else if(bulletCapacity > 0)
                {
                    magCapacity += freeMagSpace;
                    bulletCapacity = 0;
                    Console.WriteLine($"Reloading {Name} - You have {magCapacity} bullets in magasine champer and {bulletCapacity} left outside champer");
                }
                else
                {
                    Console.WriteLine("Not possible to reload - You're out of ammo");
                }
            }

        }

        public override int DoDamage()
        {
            if(magCapacity > 0)
            {
                Console.WriteLine($"Shooting {Name} with {roundsPerMinut} RPM");
                magCapacity--;
                Console.WriteLine($"You have {magCapacity} bullets in champer and {bulletCapacity} bullets outside champer");
                return baseDamage;
            }
            else
            {
                if(bulletCapacity > 0)
                {
                    Console.WriteLine("Shoot... Magasine empty - Reload!");
                }
                else
                {
                    Console.WriteLine("Shoot... You're out of ammo!");
                }
                return 0;
            }

        }

        public override string ToString()
        {
            string returnString;
            returnString  = $"Your Weapon is   : {this.GetType().Name}";
            returnString += $"\nName             : {base.Name}";
            returnString += $"\nPrice            : ${base.Price}";
            returnString += $"\nBasedamage       : {base.BaseDamage}";
            returnString += $"\nBullet Capacity  : {bulletCapacity}";
            returnString += $"\nMagazine Capacity: {magCapacity}";
            returnString += $"\nMagazine Size    : {magSize}";
            returnString += $"\nShooting Speed   : {roundsPerMinut} RPM";
            returnString += $"\nAccurate Range   : {hitRange}m";
            returnString += $"\nRecoil Level     : {recoilLevel*100}%";
            returnString += $"\nNoise Level      : {noiseLevel} dB";
            returnString += $"\nReload Time      : {reloadTime}s";
            returnString += "\n";
            return returnString;
        }
    }
}
