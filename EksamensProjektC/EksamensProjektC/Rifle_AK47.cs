﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Rifle_AK47 : AbstractRifle
    {
        public Rifle_AK47(int price, string name) : base(price, name)
        {
            baseDamage = 36;
            bulletCapacity = 90;
            magCapacity = 30;
            magSize = magCapacity;
            roundsPerMinut = 600;
            hitRange = 21.74;
            recoilLevel = 0.76;
            noiseLevel = 1992;
            reloadTime = 2.43;
        }

        public Rifle_AK47(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            bulletCapacity = 90;
            magCapacity = 30;
            magSize = magCapacity;
            roundsPerMinut = 600;
            hitRange = 21.74;
            recoilLevel = 0.76;
            noiseLevel = 1992;
            reloadTime = 2.43;
        }

        public Rifle_AK47(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : base(price, name, baseDamage, bulletCapacity, magCapacity, rmp, hitRange, recoilLevel, noiseLevel, reloadTime)
        {
        }
    }
}
