﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Grenade_Incinerate : AbstractGrenade
    {
        public Grenade_Incinerate() : this(600, "Incinerate Grenade", 1)
        {
        }

        public Grenade_Incinerate(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            effectRadius = 1;
            effectDuration = 10;
            effectDamage = 30;
        }

        public override int DoDamage()
        {
            Console.WriteLine($"You throw your {Name}");
            DoEffect();
            return effectDamage;
        }

        public override void DoEffect()
        {
            Console.WriteLine($"Your {Name} lands and incinerate all enemies for {effectDamage} in a radius of {effectRadius}m for {effectDuration}s");
        }
    }
}
