﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class ShotGun_Nova : AbstractShotGun
    {
        public ShotGun_Nova(int price, string name) : base(price, name)
        {
            baseDamage = 234;
            bulletCapacity = 32;
            magCapacity = 8;
            magSize = magCapacity;
            roundsPerMinut = 68;
            hitRange = 3.2;
            recoilLevel = 0.04;
            noiseLevel = 1992;
            reloadTime = 1.8;
        }

        public ShotGun_Nova(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            bulletCapacity = 32;
            magCapacity = 8;
            magSize = magCapacity;
            roundsPerMinut = 68;
            hitRange = 3.2;
            recoilLevel = 0.04;
            noiseLevel = 1992;
            reloadTime = 1.8;
        }

        public ShotGun_Nova(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : base(price, name, baseDamage, bulletCapacity, magCapacity, rmp, hitRange, recoilLevel, noiseLevel, reloadTime)
        {
        }
    }
}
