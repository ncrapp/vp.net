﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class UnitBuilder
    {
        public IUnit unit;

        protected AbstractKnife k1;
        protected AbstractShootingWeapon s1;
        protected AbstractShootingWeapon s2;
        protected AbstractShootingWeapon s3;
        protected AbstractGrenade g1;
        protected AbstractGrenade g2;
        protected AbstractGrenade g3;
        protected AbstractGrenade g4;

        public virtual void BuildKnife() { }
        public virtual void BuildGuns() { }
        public virtual void BuildGrenades() { }
        public abstract void addWeapon();

        public abstract IUnit Unit { get; }
    }
}
