﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class SoldierBuilder : UnitBuilder
    {
        private AbstractUnit unitInProgres;

        public SoldierBuilder(AbstractUnit unit)
        {
            unitInProgres = unit;
        }

        public override void BuildKnife()
        {
            k1 = new Knife_Huntsman("Huntsman");
        }

        public override void BuildGuns()
        {
            s1 = new Gun_Usp(200, "Usp");
            s2 = new Rifle_AWP(5000, "Magnum Sniper Rifle");
        }

        public override void BuildGrenades()
        {
            g1 = new Grenade_Flash(); ;
            g2 = new Grenade_Smoke();
        }

        public override void addWeapon()
        {
            unitInProgres.AddWeapon(k1);
            unitInProgres.AddWeapon(s1);
            unitInProgres.AddWeapon(s2);
            unitInProgres.AddWeapon(g1);
            unitInProgres.AddWeapon(g2);
        }

        public override IUnit Unit
        {
            get
            {
                return unitInProgres;
            }
        }
    }
}
