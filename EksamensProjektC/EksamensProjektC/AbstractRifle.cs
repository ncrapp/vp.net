﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractRifle : AbstractShootingWeapon
    {
        public AbstractRifle(int price, string name) : base(price, name)
        {
        }

        public AbstractRifle(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
        }

        public AbstractRifle(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : base(price, name, baseDamage, bulletCapacity, magCapacity, rmp, hitRange, recoilLevel, noiseLevel, reloadTime)
        {
        }
    }
}
