﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Grenade_Flash : AbstractGrenade
    {
        public Grenade_Flash() : this(300, "Flash Grenade", 1)
        {
        }

        public Grenade_Flash(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            effectRadius = 1;
            effectDuration = 10;
            effectDamage = 0;
        }

        public override int DoDamage()
        {
            Console.WriteLine($"You throw your {Name}");
            DoEffect();
            return baseDamage;
        }

        public override void DoEffect()
        {
            Console.WriteLine($"Your {Name} lands and blinds all enemies in a radius of {effectRadius}m for {effectDuration}s");
        }
    }
}
