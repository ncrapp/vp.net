﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractUnit : IUnit, IMoveable
    {        
        private bool isAlive;
        private int health;
        private string unitName;
        private double damageAmp;
        private IWeapon equippedWeapon;
        private int weaponIndex;
        private int grenadeAmount;
        private int shootingWeapAmount;
        private int knifeAmount;
        private int maxGrenadeSlot;
        private int maxShootingWeaponSlot;
        private int maxKnifeSlot;
        protected List<IWeapon> weaponSlots;

        public AbstractUnit(string unitName, double damageAmp) : this(unitName, damageAmp, 4, 3)
        {
        }

        public AbstractUnit(string unitName, double damageAmp, int maxGrenade, int maxShootingWeapon)
        {
            this.unitName = unitName;
            this.grenadeAmount = 0;
            this.shootingWeapAmount = 0;
            this.knifeAmount = 0;
            this.maxGrenadeSlot = maxGrenade;
            this.maxShootingWeaponSlot = maxShootingWeapon;
            this.maxKnifeSlot = 1;
            this.isAlive = true;
            this.health = 100;
            this.damageAmp = damageAmp;
            this.weaponIndex = 0;
            this.weaponSlots = new List<IWeapon>();            
        }

        public IWeapon EquippedWeapon { get { return equippedWeapon; } }
        public List<IWeapon> WeaponSlots { get { return weaponSlots;  } }
        public int Health { get { return health; } }
        public double DamageAmp { get { return damageAmp; } }
        public bool IsAlive { get { return isAlive; } }

        public virtual void AddWeapon(IWeapon weapon)
        {            
            if (weapon is AbstractGrenade)
            {
                if(grenadeAmount < maxGrenadeSlot)
                {
                    weaponSlots.Add(weapon);
                    grenadeAmount++;
                } 
                else
                    Console.WriteLine($"Max capacity for {weapon.GetType().Name} in weaponslot has been reached");
            }
            else if(weapon is AbstractShootingWeapon)
            {
                if(shootingWeapAmount < maxShootingWeaponSlot)
                {
                    weaponSlots.Add(weapon);
                    shootingWeapAmount++;
                }
                else
                    Console.WriteLine($"Max capacity for {weapon.GetType().Name} in weaponslot has been reached");
            }
            else if(weapon is AbstractKnife)
            {
                if(knifeAmount < maxKnifeSlot)
                {
                    weaponSlots.Add(weapon);
                    knifeAmount++;
                }
                else
                    Console.WriteLine($"Max capacity for {weapon.GetType().Name} in weaponslot has been reached");
            }
            else
            {
                Console.WriteLine($"Not possible to add {weapon.GetType()} into {this.GetType().Name}");
            }
            this.equippedWeapon = weaponSlots[0];

        }

        public double Attack()
        {
            try
            {
                double attackDmg;
                if (equippedWeapon is AbstractGrenade)
                {
                    attackDmg = equippedWeapon.DoDamage();
                    weaponSlots.Remove(equippedWeapon);
                    equippedWeapon = weaponSlots[1];
                    weaponIndex = 1;
                    Console.WriteLine($"You now have {equippedWeapon.Name} equiped");
                }
                else
                {
                    attackDmg = equippedWeapon.DoDamage();
                    attackDmg += (attackDmg * damageAmp);
                }
                Console.WriteLine($"You are a {unitName} with {damageAmp*100}% damage amplifier so you do {attackDmg} damage to your enemy with {equippedWeapon.Name}");
                return attackDmg;
            }
                catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message}");
                return 0;
            }
        }

        public void ToggleSecondary()
        {
            try
            {
                if (equippedWeapon is ISecondarable)
                {
                    ISecondarable secondarableWeapon = equippedWeapon as ISecondarable;
                    secondarableWeapon.ToggleSecondaryEffect();
                    equippedWeapon = secondarableWeapon as IWeapon;
                }
                else
                {
                    Console.WriteLine($"No secondary functionality available for {equippedWeapon.Name}");
                }
            }
            catch (Exception e)
            {

                Console.WriteLine($"Exception: {e.Message}");
            }
        }

        public void ReloadWeapon()
        {
            try
            {
                AbstractShootingWeapon selectedWeapon = EquippedWeapon as AbstractShootingWeapon;
                Console.WriteLine($"Amount of bullets in magasine: {selectedWeapon.MagCapacity}");
                selectedWeapon.Reload();
            }
            catch (Exception)
            {
                Console.WriteLine($"Not possible to reload {equippedWeapon.Name}");
            }
        }

        public void switchNextWeapon()
        {
            if (weaponIndex < weaponSlots.Count - 1)
            {
                weaponIndex++;
                equippedWeapon = weaponSlots[weaponIndex];
                Console.WriteLine($"You equipped {equippedWeapon.Name}");
            }
            else
            {
                Console.WriteLine("Last weapon already equiped");
            }
        }

        public void switchPrevWeapon()
        {
            if (weaponIndex > 0)
            {
                weaponIndex--;
                equippedWeapon = weaponSlots[weaponIndex];
                Console.WriteLine($"You equipped {equippedWeapon.Name}");
            }
            else
            {
                Console.WriteLine("First weapon already equiped");
            }
        }

        public void TakeDamage(int incommingDmg)
        {
            if (this.health > 0)
            {
                Console.WriteLine($"Your taking {incommingDmg} damage and you health is now {this.health - incommingDmg}");
                this.health -= incommingDmg;                
                if (this.health <= 0)
                {
                    Console.WriteLine($"You took {incommingDmg} damage and died");
                    this.isAlive = false;
                }
            }
            else
            {
                Console.WriteLine("You're already dead!");
            }
        }

        public void moveForward()
        {
            Console.WriteLine("You move forward");
        }

        public void moveBackwards()
        {
            Console.WriteLine("You move backwards");
        }

        public void moveLeft()
        {
            Console.WriteLine("You strafe to the left");
        }

        public void moveRight()
        {
            Console.WriteLine("You strafe to the right");

        }

        public override string ToString()
        {
            string returnString = $"Your Unit is a {unitName} and have {damageAmp} damage amplifier";
            return returnString;
        }
    }
}
