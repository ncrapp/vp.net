﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class MarineBuilder : UnitBuilder
    {
        private AbstractUnit unitInProgres;

        public MarineBuilder(AbstractUnit unit)
        {
            unitInProgres = unit;
        }

        public override void BuildKnife()
        {
            k1 = new Knife_Butterfly("Butterfly Knife");
        }

        public override void BuildGuns()
        {
            s1 = new Rifle_AK47(2700, "AK-47");
        }

        public override void BuildGrenades()
        {
            g1 = new Grenade_Frag();;
            g2 = new Grenade_Incinerate();
        }

        public override void addWeapon()
        {
            unitInProgres.AddWeapon(k1);
            unitInProgres.AddWeapon(s1);
            unitInProgres.AddWeapon(g1);
            unitInProgres.AddWeapon(g2);
        }

        public override IUnit Unit
        {
            get
            {
                return unitInProgres;
            }
        }
    }
}
