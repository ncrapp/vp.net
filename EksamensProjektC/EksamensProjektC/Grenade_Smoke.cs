﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Grenade_Smoke : AbstractGrenade
    {
        public Grenade_Smoke() : this(300, "Smoke Grenade", 1)
        {
        }

        public Grenade_Smoke(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            effectRadius = 1;
            effectDuration = 30;
            effectDamage = 0;
        }

        public override int DoDamage()
        {
            Console.WriteLine($"You throw your {Name}");
            DoEffect();
            return baseDamage;
        }

        public override void DoEffect()
        {
            Console.WriteLine($"Your {Name} lands and covers a radius of {effectRadius}meters with smoke for {effectDuration}seconds ");
        }
    }
}
