﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public interface IMoveable
    {
        void moveForward();
        void moveBackwards();
        void moveLeft();
        void moveRight();
    }
}
