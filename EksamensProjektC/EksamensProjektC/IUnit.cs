﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public interface IUnit
    {
        IWeapon EquippedWeapon { get; }
        List<IWeapon> WeaponSlots { get; }
        int Health { get; }
        double DamageAmp { get; }
        bool IsAlive { get; }

        void AddWeapon(IWeapon weapon);
        void TakeDamage(int incommingDmg);
        double Attack();
        void ToggleSecondary();
        void ReloadWeapon();
        void switchNextWeapon();
        void switchPrevWeapon();
    }
}
