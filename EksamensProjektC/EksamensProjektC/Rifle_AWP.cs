﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    class Rifle_AWP : AbstractRifle, ISecondarable
    {
        private bool secondaryEffect = false;
        private string effectName = "sniper";

        public Rifle_AWP(int price, string name) : base(price, name)
        {
            baseDamage = 115;
            bulletCapacity = 30;
            magCapacity = 10;
            magSize = magCapacity;
            roundsPerMinut = 41;
            hitRange = 20.5;
            recoilLevel = 0.03;
            noiseLevel = 7000;
            reloadTime = 3.6;
        }

        public Rifle_AWP(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            bulletCapacity = 30;
            magCapacity = 10;
            magSize = magCapacity;
            roundsPerMinut = 41;
            hitRange = 20.5;
            recoilLevel = 0.03;
            noiseLevel = 7000;
            reloadTime = 3.6;
        }

        public Rifle_AWP(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : base(price, name, baseDamage, bulletCapacity, magCapacity, rmp, hitRange, recoilLevel, noiseLevel, reloadTime)
        {
        }

        public bool SecondaryEffect { get => secondaryEffect; }
        public string EffectName { get => effectName; }

        public void ToggleSecondaryEffect()
        {
            if (!secondaryEffect)
            {
                Console.WriteLine($"Enabling {effectName} on {Name}");
                hitRange = 69;
                Console.WriteLine($"Your hitrange is now {hitRange}");
                secondaryEffect = true;
            }
            else
            {
                Console.WriteLine($"Disabling {effectName} on {Name}");
                hitRange = 20.5;
                Console.WriteLine($"Your hitrange is now {hitRange}");
                secondaryEffect = false;
            }
        }
    }
}
