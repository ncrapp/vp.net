﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class UnitDirector
    {
        public abstract IUnit Build(UnitBuilder builder);
    }
}
