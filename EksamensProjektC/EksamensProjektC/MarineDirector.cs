﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class MarineDirector : UnitDirector
    {
        public override IUnit Build(UnitBuilder builder)
        {
            builder.BuildKnife();
            builder.BuildGuns();
            builder.BuildGrenades();
            builder.addWeapon();
            return builder.Unit;
        }
    }
}
