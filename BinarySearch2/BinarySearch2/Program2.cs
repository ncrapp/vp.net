﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearch2
{
    class Program2
    {
        // A recursive binary search  
        // function. It returns location 
        // of target in given list arr[l..r]  
        // is present, otherwise -1 
        static int binarySearch(List<int> arr, int left, int right, int target)
        {
            if (right < left)
                return -1;

            int mid = left + (right - left) / 2;

            // If the element is present  
            // at the middle itself 
            if (arr[mid] == target)
                return mid;

            // If element is smaller than  
            // mid, then it can only be  
            // present in left subarray 
            if (arr[mid] > target)
                return binarySearch(arr, left, mid - 1, target);

            // Else the element  
            // can only be present 
            // in right subarray 
            return binarySearch(arr, mid + 1, right, target);
        }

        // Returns list of elements in list that matches target  
        static List<int> countOccurrences(List<int> arr, int n, int target)
        {
            List<int> matches = new List<int>();

            int ind = binarySearch(arr, 0, n - 1, target);

            // If element is not present 
            if (ind == -1)
                return matches;

            // Count elements on left side if an element is present.             
            matches.Add(target);
            int left = ind - 1;
            while (left >= 0 && arr[left] == target)
            {
                matches.Add(target);
                left--;
            }

            // Count elements on right side. 
            int right = ind + 1;
            while (right < n && arr[right] == target)
            {
                matches.Add(target);
                right++;
            }

            return matches;
        }


        // Driver code 
        public static void Main()
        {

            List<int> arr = new List<int> {1, 1, 2, 3, 3, 3, 4, 7, 8, 8};
            int n = arr.Count; //Lenght of List
            int x = 8;            
            List<int> matches = countOccurrences(arr, n, x);
            if (matches.Any())
            {
                foreach (int i in matches)
                {
                    Console.WriteLine("Tal fundet i I: {0}", i);
                }
            }
            else
                Console.WriteLine("No search hits");

            Console.ReadLine();
        }
    }
}
