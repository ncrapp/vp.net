﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearch2
{
    class Program
    {
        // A recursive binary search  
        // function. It returns location 
        // of x in given array arr[l..r]  
        // is present, otherwise -1 
        static int binarySearch(List<int> arr, int l, int r, int x)
        {
            if (r < l)
                return -1;

            int mid = l + (r - l) / 2;

            // If the element is present  
            // at the middle itself 
            if (arr[mid] == x)
                return mid;

            // If element is smaller than  
            // mid, then it can only be  
            // present in left subarray 
            if (arr[mid] > x)
                return binarySearch(arr, l, mid - 1, x);

            // Else the element  
            // can only be present 
            // in right subarray 
            return binarySearch(arr, mid + 1, r, x);
        }

        // Returns number of times x  
        // occurs in arr[0..n-1] 
        static List<int> countOccurrences(List<int> arr, int n, int x)
        {
            List<int> matches = new List<int>();

            int ind = binarySearch(arr, 0, n - 1, x);

            // If element is not present 
            if (ind == -1)
                return null;

            // Count elements on left side. 
            int count = 1;
            int left = ind - 1;
            while (left >= 0 && arr[left] == x)
            {
                count++;
                matches.Add(x);
                left--;
            }

            // Count elements on right side. 
            int right = ind + 1;
            while (right < n && arr[right] == x)
            {
                count++;
                matches.Add(x);
                right++;
            }

            return matches;
        }


        // Driver code 
        public static void Main2()
        {
            List<int> arr = new List<int>();
            foreach (int number in new int[] { 1, 2, 2, 2, 3, 3, 4, 7, 8, 8 })
            {
                arr.Add(number);
            }
            
            int n = arr.Count;
            int x = 2;
            //Console.Write(countOccurrences(arr, n, x));
            foreach(int i in countOccurrences(arr, n, x))
            {
                Console.WriteLine("Tal fundet i I: {0}", i);
            }

            Console.ReadLine();
        }
    }
}
