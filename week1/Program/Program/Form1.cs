﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "Hello World!";
            textBox2.Text = "Hello OtherWorld";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String a = textBox1.Text;
            String b = textBox2.Text;

            textBox1.Text = b;
            textBox2.Text = a;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
