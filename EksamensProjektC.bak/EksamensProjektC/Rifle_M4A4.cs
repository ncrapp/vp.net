﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Rifle_M4A4 : AbstractRifle
    {
        public Rifle_M4A4(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
            bulletCapacity = 90;
            magCapacity = 30;
            roundsPerMinut = 666;
            hitRange = 28;
            recoilLevel = 0.76;
            noiseLevel = 1992;
            reloadTime = 3.1;
        }

        public Rifle_M4A4(int price, string name) : base(price, name)
        {
            BaseDamage = 33;
            bulletCapacity = 90;
            magCapacity = 30;
            roundsPerMinut = 666;
            hitRange = 28;
            recoilLevel = 0.76;
            noiseLevel = 1992;
            reloadTime = 3.1;
        }

        public Rifle_M4A4(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : base(price, name, baseDamage, bulletCapacity, magCapacity, rmp, hitRange, recoilLevel, noiseLevel, reloadTime)
        {
        }
    }
}
