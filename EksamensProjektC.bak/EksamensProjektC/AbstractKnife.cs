﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractKnife : AbstractWeapon
    {
        public AbstractKnife(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
        }
    }
}
