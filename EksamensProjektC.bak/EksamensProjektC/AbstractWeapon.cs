﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractWeapon : IWeapon
    {
        private int price;
        private string name;
        private int baseDamage;

        public AbstractWeapon(int price, string name, int baseDamage)
        {
            this.price = price;
            this.name = name;
            this.BaseDamage = baseDamage;
        }

        public int Price { get => price;  }
        public string Name { get => name; }
        public int BaseDamage { get => baseDamage; set => baseDamage = value; }

        public abstract int DoDamage();

        public override string ToString()
        {
            string returnString = $"Your Weapon is a {this.GetType().Name} called {name} - Price is {price} and it's basedamage is {BaseDamage}";
            return returnString;
        }
    }
}
