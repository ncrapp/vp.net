﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public abstract class AbstractGrenade : AbstractWeapon
    {
        private string name;
        private int effectRadius;
        private int effectDuration;

        public AbstractGrenade(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
        }

        public abstract void DoEffect();
    }
}
