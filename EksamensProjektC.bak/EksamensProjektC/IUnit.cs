﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public interface IUnit
    {
        IWeapon SelectedWeapon { get; }
        IArmor Armor { get; }
        IWeapon[] WeaponSlots { get; }
        int Health { get; }
        int DmgAmp { get; }
        bool isAlive { get; }

        void GetDmg(int incDmg);
        int Attack();
        void switchWeapon(IWeapon nextWeapon);
    }
}
