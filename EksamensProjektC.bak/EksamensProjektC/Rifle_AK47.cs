﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public class Rifle_AK47 : AbstractRifle
    {
        public Rifle_AK47(int price, string name) : base(price, name)
        {
        }

        public Rifle_AK47(int price, string name, int baseDamage) : base(price, name, baseDamage)
        {
        }

        public Rifle_AK47(int price, string name, int baseDamage, int bulletCapacity, int magCapacity, int rmp, double hitRange, double recoilLevel, double noiseLevel, double reloadTime) : base(price, name, baseDamage, bulletCapacity, magCapacity, rmp, hitRange, recoilLevel, noiseLevel, reloadTime)
        {
        }
    }
}
