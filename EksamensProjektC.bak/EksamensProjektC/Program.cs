﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractUnit unit = new Unit_Marine();
            UnitBuilder builder = new MarineBuilder(unit);
            UnitDirector director = new MarineDirector();
            IUnit u = director.Build(builder);
            Console.WriteLine(u);


            AbstractShootingWeapon w1 = new Rifle_AK47(2700, "AK-47", 36);
            Console.WriteLine(w1.ToString());


            AbstractShootingWeapon w2 = new Rifle_M4A4(3100, "M4A4");
            Console.WriteLine(w2.ToString());


            AbstractShootingWeapon w3 = new Rifle_AK47(2700, "AK-47", 36, 90, 30, 600, 21.74, 0.69, 1992, 2.43);
            Console.WriteLine(w3.ToString());



            Console.ReadLine();

        }
    }
}
