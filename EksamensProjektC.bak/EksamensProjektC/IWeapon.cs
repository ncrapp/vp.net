﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EksamensProjektC
{
    public interface IWeapon
    {
        string Name { get; }
        int Price { get; }
        int DoDamage();
    }
}
